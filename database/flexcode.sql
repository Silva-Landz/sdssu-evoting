-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 17, 2018 at 04:08 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flexcode`
--

-- --------------------------------------------------------

--
-- Table structure for table `demo_device`
--

CREATE TABLE `demo_device` (
  `device_name` varchar(50) NOT NULL,
  `sn` varchar(50) NOT NULL,
  `vc` varchar(50) NOT NULL,
  `ac` varchar(50) NOT NULL,
  `vkey` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_device`
--

INSERT INTO `demo_device` (`device_name`, `sn`, `vc`, `ac`, `vkey`) VALUES
('SDSSU device 1', 'GZ00E012821', '4049645FA2496AD', 'HXB823521FD890E9AEA81EPB', 'E915940F88AF5B8B5681348575484866');

-- --------------------------------------------------------

--
-- Table structure for table `demo_finger`
--

CREATE TABLE `demo_finger` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `finger_id` int(11) UNSIGNED NOT NULL,
  `finger_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `demo_log`
--

CREATE TABLE `demo_log` (
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_name` varchar(50) NOT NULL,
  `data` text NOT NULL COMMENT 'sn+pc time'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_log`
--

INSERT INTO `demo_log` (`log_time`, `user_name`, `data`) VALUES
('2018-05-30 15:24:17', 'ted', '2018-05-30 23:24:17 (PC Time) | GZ00E012821 (SN)'),
('2018-05-30 15:54:02', 'ted', '2018-05-30 23:54:01 (PC Time) | GZ00E012821 (SN)'),
('2018-05-30 15:54:24', 'Landrex', '2018-05-30 23:54:24 (PC Time) | GZ00E012821 (SN)'),
('2018-05-30 15:54:58', 'Landrex', '2018-05-30 23:54:58 (PC Time) | GZ00E012821 (SN)'),
('2018-05-30 16:37:16', 'landz', '2018-05-31 00:37:16 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 01:55:08', 'landz', '2018-05-31 09:55:08 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 13:33:17', 'Jonna', '2018-05-31 21:33:17 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 13:44:11', 'landrex', '2018-05-31 21:44:11 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 14:17:53', 'landrex', '2018-05-31 22:17:53 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 14:19:55', 'landrex', '2018-05-31 22:19:55 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 14:34:47', 'landrex', '2018-05-31 22:34:47 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 15:17:36', 'sunday', '2018-05-31 23:17:36 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 15:27:24', 'sunday', '2018-05-31 23:27:24 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 15:27:36', 'landrex', '2018-05-31 23:27:36 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 15:29:37', 'john', '2018-05-31 23:29:37 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 16:10:50', 'john', '2018-06-01 00:10:50 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 16:16:56', 'landrex', '2018-06-01 00:16:56 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 16:21:11', 'landrex', '2018-06-01 00:21:11 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 17:22:38', 'landrex', '2018-06-01 01:22:38 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 19:37:55', 'ikaw', '2018-06-01 03:37:52 (PC Time) | GZ00E012821 (SN)'),
('2018-05-31 19:40:06', 'ikaw', '2018-06-01 03:40:06 (PC Time) | GZ00E012821 (SN)'),
('2018-06-01 04:57:07', 'ted', '2018-06-01 12:57:07 (PC Time) | GZ00E012821 (SN)'),
('2018-06-01 05:00:41', 'landrex', '2018-06-01 13:00:41 (PC Time) | GZ00E012821 (SN)'),
('2018-06-01 05:03:34', 'sunday', '2018-06-01 13:03:34 (PC Time) | GZ00E012821 (SN)'),
('2018-06-01 05:18:53', 'ted', '2018-06-01 13:18:53 (PC Time) | GZ00E012821 (SN)'),
('2018-06-01 05:27:12', 'ted', '2018-06-01 13:27:09 (PC Time) | GZ00E012821 (SN)'),
('2018-06-01 05:28:51', 'jeffrey', '2018-06-01 13:28:51 (PC Time) | GZ00E012821 (SN)'),
('2018-06-02 02:43:56', 'landrex', '2018-06-02 10:43:56 (PC Time) | GZ00E012821 (SN)'),
('2018-06-05 18:52:19', 'landrex', '2018-06-06 02:52:19 (PC Time) | GZ00E012821 (SN)');

-- --------------------------------------------------------

--
-- Table structure for table `demo_user`
--

CREATE TABLE `demo_user` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `user_name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sdssu_candidates`
--

CREATE TABLE `sdssu_candidates` (
  `can_id` int(11) NOT NULL,
  `pos_id` int(11) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `deg_id` int(11) DEFAULT NULL,
  `year_lvl` enum('1st','2nd','3rd','4th','5th') NOT NULL,
  `party` enum('WISELY','CHANGES') NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sdssu_candidates`
--

INSERT INTO `sdssu_candidates` (`can_id`, `pos_id`, `fname`, `lname`, `mname`, `gender`, `deg_id`, `year_lvl`, `party`, `image`) VALUES
(1, 7, 'Landrex', 'Rebuera', 'Orozco', 'Male', 2, '4th', 'CHANGES', 'dummy-man-570x570.png'),
(2, 1, 'Geneva', 'Lawson', 'A', 'Female', 1, '1st', 'WISELY', 'dummy-woman-570x570.png'),
(3, 1, 'Jonna', 'Tabada', 'Coma', 'Female', 2, '3rd', 'CHANGES', 'dummy-woman-570x570.png'),
(4, 2, 'Noah', 'Cannon', 'C', 'Male', 3, '4th', 'WISELY', 'dummy-man-570x570.png'),
(5, 2, 'Dean', 'Phillips', 'D', 'Male', 1, '2nd', 'CHANGES', 'dummy-man-570x570.png'),
(6, 3, 'Patsy', 'Wong', 'E', 'Male', 3, '1st', 'WISELY', 'dummy-man-570x570.png'),
(7, 3, 'Stella', 'Austin', 'F', 'Female', 2, '1st', 'CHANGES', 'dummy-woman-570x570.png'),
(8, 4, 'Crystal', 'Douglas', 'F', 'Male', 1, '2nd', 'CHANGES', 'dummy-man-570x570.png'),
(9, 4, 'Toni', 'Jimenez', 'G', 'Female', 3, '3rd', 'WISELY', 'dummy-woman-570x570.png'),
(10, 5, 'Merle', 'Bell', 'H', 'Female', 2, '4th', 'WISELY', 'dummy-woman-570x570.png'),
(11, 5, 'Willie', 'Ford', 'I', 'Male', 1, '5th', 'CHANGES', 'dummy-man-570x570.png'),
(12, 6, 'Della', 'Kennedy', 'J', 'Female', 2, '5th', 'WISELY', 'dummy-woman-570x570.png'),
(13, 6, 'Whitney', 'Baker', 'K', 'Female', 3, '2nd', 'CHANGES', 'dummy-woman-570x570.png'),
(14, 7, 'Frankie', 'Chapman', 'L', 'Male', 1, '4th', 'WISELY', 'dummy-man-570x570.png'),
(15, 7, 'Boyd', 'Vasquez', 'M', 'Male', 2, '5th', 'CHANGES', 'dummy-man-570x570.png'),
(16, 7, 'Lynette', 'Anderson', 'N', 'Female', 3, '3rd', 'WISELY', 'dummy-woman-570x570.png'),
(17, 7, 'Howard', 'Gomez', 'O', 'Male', 2, '1st', 'CHANGES', 'dummy-man-570x570.png'),
(18, 7, 'Tasha', 'Welch', 'P', 'Female', 3, '2nd', 'WISELY', 'dummy-woman-570x570.png'),
(19, 7, 'Belinda', 'Clark', 'Q', 'Female', 3, '4th', 'CHANGES', 'dummy-woman-570x570.png'),
(20, 7, 'Jose', 'Turner', 'R', 'Male', 1, '4th', 'CHANGES', 'dummy-man-570x570.png'),
(21, 7, 'Tambra', 'Bentler', 'Z', 'Female', 2, '3rd', 'CHANGES', 'dummy-woman-570x570.png');

-- --------------------------------------------------------

--
-- Table structure for table `sdssu_degrees`
--

CREATE TABLE `sdssu_degrees` (
  `deg_id` int(11) NOT NULL,
  `deg_code` varchar(50) NOT NULL,
  `deg_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sdssu_degrees`
--

INSERT INTO `sdssu_degrees` (`deg_id`, `deg_code`, `deg_name`) VALUES
(1, 'BSCS', 'Bachelors of Science in Computer Science'),
(2, 'BSInfoTech', 'Bachelors of Science in Information Technology'),
(3, 'BSED', 'Bachelors of Science in Secondary Education');

-- --------------------------------------------------------

--
-- Table structure for table `sdssu_log`
--

CREATE TABLE `sdssu_log` (
  `log_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sdssu_log`
--

INSERT INTO `sdssu_log` (`log_id`, `user_id`, `action`, `date`) VALUES
(1, 1, 'Logged out', '2018-06-01 07:10:07'),
(2, 1, 'Logged in', '2018-06-01 07:10:10'),
(3, 1, 'Inserted a new candidate', '2018-06-01 07:14:05'),
(4, 1, 'Inserted a new voter', '2018-06-01 07:40:04'),
(5, 1, 'Deleted a candidate', '2018-06-01 07:51:19'),
(6, 1, 'Logged in', '2018-06-02 02:35:40'),
(7, 1, 'Inserted a new voter', '2018-06-02 02:39:22'),
(8, 1, 'Inserted a new voter', '2018-06-02 02:39:56'),
(9, 1, 'Inserted a new voter', '2018-06-02 02:43:07'),
(10, 1, 'Logged in', '2018-06-02 02:45:39'),
(11, 1, 'Deleted a voter', '2018-06-02 02:46:11'),
(12, 1, 'Inserted a new voter', '2018-06-02 03:42:53'),
(13, 1, 'Logged in', '2018-07-11 05:59:52'),
(14, 1, 'Logged out', '2018-07-11 11:01:25'),
(15, 1, 'Logged in', '2018-07-18 12:20:50'),
(16, 1, 'Logged in', '2018-07-18 17:09:13'),
(17, 1, 'Logged in', '2018-07-21 03:35:19'),
(18, 1, 'Logged in', '2018-08-01 04:59:56'),
(19, 1, 'Deleted a voter', '2018-08-01 05:01:12'),
(20, 1, 'Logged in', '2018-08-17 01:57:17'),
(21, 1, 'Logged out', '2018-08-17 02:00:53');

-- --------------------------------------------------------

--
-- Table structure for table `sdssu_positions`
--

CREATE TABLE `sdssu_positions` (
  `pos_id` int(11) NOT NULL,
  `pos_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sdssu_positions`
--

INSERT INTO `sdssu_positions` (`pos_id`, `pos_type`) VALUES
(1, 'President'),
(2, 'Vice President - Internal'),
(3, 'Vice President - External'),
(4, 'Secretary'),
(5, 'Treasurer'),
(6, 'Auditor'),
(7, 'Senator');

-- --------------------------------------------------------

--
-- Table structure for table `sdssu_users`
--

CREATE TABLE `sdssu_users` (
  `user_id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sdssu_users`
--

INSERT INTO `sdssu_users` (`user_id`, `fname`, `lname`, `username`, `password`, `user_type`) VALUES
(1, 'Landrex', 'Rebuera', 'admin', 'admin', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `sdssu_voters`
--

CREATE TABLE `sdssu_voters` (
  `voters_id` int(11) NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `deg_id` int(11) DEFAULT NULL,
  `year_lvl` enum('1st','2nd','3rd','4th','5th') DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sdssu_votes`
--

CREATE TABLE `sdssu_votes` (
  `votes_id` int(11) NOT NULL,
  `can_id` int(11) DEFAULT NULL,
  `total_votes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `demo_device`
--
ALTER TABLE `demo_device`
  ADD PRIMARY KEY (`sn`);

--
-- Indexes for table `demo_finger`
--
ALTER TABLE `demo_finger`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `demo_log`
--
ALTER TABLE `demo_log`
  ADD PRIMARY KEY (`log_time`);

--
-- Indexes for table `demo_user`
--
ALTER TABLE `demo_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `sdssu_candidates`
--
ALTER TABLE `sdssu_candidates`
  ADD PRIMARY KEY (`can_id`);

--
-- Indexes for table `sdssu_degrees`
--
ALTER TABLE `sdssu_degrees`
  ADD PRIMARY KEY (`deg_id`);

--
-- Indexes for table `sdssu_log`
--
ALTER TABLE `sdssu_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `sdssu_positions`
--
ALTER TABLE `sdssu_positions`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `sdssu_users`
--
ALTER TABLE `sdssu_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `sdssu_voters`
--
ALTER TABLE `sdssu_voters`
  ADD PRIMARY KEY (`voters_id`);

--
-- Indexes for table `sdssu_votes`
--
ALTER TABLE `sdssu_votes`
  ADD PRIMARY KEY (`votes_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `demo_user`
--
ALTER TABLE `demo_user`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sdssu_candidates`
--
ALTER TABLE `sdssu_candidates`
  MODIFY `can_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `sdssu_degrees`
--
ALTER TABLE `sdssu_degrees`
  MODIFY `deg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sdssu_log`
--
ALTER TABLE `sdssu_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `sdssu_positions`
--
ALTER TABLE `sdssu_positions`
  MODIFY `pos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sdssu_users`
--
ALTER TABLE `sdssu_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sdssu_voters`
--
ALTER TABLE `sdssu_voters`
  MODIFY `voters_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sdssu_votes`
--
ALTER TABLE `sdssu_votes`
  MODIFY `votes_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
