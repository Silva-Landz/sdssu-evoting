<?php session_start(); ?>

<?php include 'includes/header.php'; ?>

<script src="assets/js/jquery.js" crossorigin="anonymous"></script>
<script src="assets/js/sweetalert2.all.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function() {
	        swal({
	            type: 'error',
				title: 'Oops. . .',
				text: 'Something went wrong!'
	        }).then(function(isConfirm){
	        	if (isConfirm) {
	        		window.location = "verify";
	        	}
	        });
	    }, 100);
	});
</script>
