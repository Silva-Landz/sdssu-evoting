	
<?php 
	// session_start();	

	include 'verify/include/global.php';
	include 'includes/function.php';
	include 'includes/header.php'; 

?>

<div class="container-fluid bread-fluid">
	<nav class="navbar navbar-dark bg-primary">
		<a class="navbar-brand" href="/sdssuevote/verify"><img src="assets/images/sdssulogo.png" width="45" height="45"></a>
	</nav>
</div>

<?php if (isset($_GET['voters_id']) && isset($_GET['status']) && $_GET['status'] == 0): ?>

		<?php if (count($_POST['senator']) <= 9  && count($_POST['senator']) >= 0): ?>
					<form method="POST" action="add_votes.php" id="review_ballot">
					<div class="container">
						<div class="row mt-4 justify-content-center">
							<div class="col-sm-10">
								<div class="card">
									<div class="card-header">
										<h5><span class="fa fa-check-circle"></span> Review Ballot</h5>
									</div>
									<div class="card-body">
										<div class="row">
											<input type="hidden" name="voters_id" value="<?=$_POST['voters_id']?>">
											<div class="col-sm-4"><h5>President</h5></div>
											<div class="col-sm-8">
												<?php if (empty($_POST['president'])): ?>
														<h5>No candidate . . .</h5>
														<input type="hidden" name="president" value="">
													<?php else: ?>
														<?php 
															$candidate = getSpecificCan($_POST['president']); 
															foreach ($candidate as $row) {
																echo "<h5>".$row['fname']." ".$row['mname']." ".$row['lname']."</h5>";
															}
														?>
														<input type="hidden" name="president" value="<?=$_POST['president']?>">
												<?php endif ?>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4"><h5>Vice President - Internal:</h5></div>
											<div class="col-sm-8">
												<?php if (empty($_POST['viceInternal'])): ?>
														<h5>No candidate . . .</h5>
														<input type="hidden" name="viceInternal" value="">
													<?php else: ?>
														<?php 
															$candidate = getSpecificCan($_POST['viceInternal']); 
															foreach ($candidate as $row) {
																echo "<h5>".$row['fname']." ".$row['mname']." ".$row['lname']."</h5>";
															}
														?>
														<input type="hidden" name="viceInternal" value="<?=$_POST['viceInternal']?>">
												<?php endif ?>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4"><h5>Vice President - External:</h5></div>
											<div class="col-sm-8">
												<?php if (empty($_POST['viceExternal'])): ?>
														<h5>No candidate . . .</h5>
														<input type="hidden" name="viceExternal" value="">
													<?php else: ?>
														<?php 
															$candidate = getSpecificCan($_POST['viceExternal']); 
															foreach ($candidate as $row) {
																echo "<h5>".$row['fname']." ".$row['mname']." ".$row['lname']."</h5>";
															}
														?>
														<input type="hidden" name="viceExternal" value="<?=$_POST['viceExternal']?>">
												<?php endif ?>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4"><h5>Secretary: </h5></div>
											<div class="col-sm-8">
												<?php if (empty($_POST['secretary'])): ?>
														<h5>No candidate . . .</h5>
														<input type="hidden" name="secretary" value="">
													<?php else: ?>
														<?php 
															$candidate = getSpecificCan($_POST['secretary']); 
															foreach ($candidate as $row) {
																echo "<h5>".$row['fname']." ".$row['mname']." ".$row['lname']."</h5>";
															}
														?>
														<input type="hidden" name="secretary" value="<?=$_POST['secretary']?>">
												<?php endif ?>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4"><h5>Treasurer: </h5></div>
											<div class="col-sm-8">
												<?php if (empty($_POST['treasurer'])): ?>
														<h5>No candidate . . .</h5>
														<input type="hidden" name="treasurer" value="">
													<?php else: ?>
														<?php 
															$candidate = getSpecificCan($_POST['treasurer']); 
															foreach ($candidate as $row) {
																echo "<h5>".$row['fname']." ".$row['mname']." ".$row['lname']."</h5>";
															}
														?>
														<input type="hidden" name="treasurer" value="<?=$_POST['treasurer']?>">
												<?php endif ?>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4"><h5>Auditor: </h5></div>
											<div class="col-sm-8">
												<?php if (empty($_POST['auditor'])): ?>
														<h5>No candidate . . .</h5>
														<input type="hidden" name="auditor" value="">
													<?php else: ?>
														<?php 
															$candidate = getSpecificCan($_POST['auditor']); 
															foreach ($candidate as $row) {
																echo "<h5>".$row['fname']." ".$row['mname']." ".$row['lname']."</h5>";
															}
														?>
														<input type="hidden" name="auditor" value="<?=$_POST['auditor']?>">
												<?php endif ?>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-4"><h5>Senator(s): </h5></div>
											<div class="col-sm-8">
												<?php if (isset($_POST['senator']) &&  sizeof($_POST['senator']) == 1): ?>
															<h5>No candidate. . .</h5>
															<input type="checkbox" name="senator[]" value="" style="display: none">
												<?php else: ?>
														<?php 
															$ctr = 0;
															foreach ($_POST['senator'] as $senator): ?>
																<?php if ($ctr < 9): ?>
																		<?php 
																			$candidate = getSpecificCan($senator); 
																			foreach ($candidate as $row) {
																				echo "<h5>".$row['fname']." ".$row['mname']." ".$row['lname']."</h5>";
																			}
																		?>
																		<input type="checkbox" name="senator[]" value="<?=$senator?>" checked="checked" style="display: none">
																<?php else: ?>
																		<h5 class="text text-danger">You've chose more than 8 senators already. Back to voting page.</h5>
																<?php endif; $ctr++; ?>
														<?php endforeach ?>
												<?php endif ?>

											</div>
										</div>
										</div>
									</div>
									<div class="card-footer">
										<button type="button" class="btn btn-default" onclick="history.back();"><span class="fa fa-arrow-left"></span> Back</button>
										<?php if (isset($_GET['voters_id']) && $_GET['status'] == 0): ?>
												<?php echo $btn ='<button type="submit" class="btn btn-primary pull-right" id="submit_vote">Submit final votes</button>'; ?>
										<?php else: ?>
												<?php include 'includes/403error.php'; ?>
										<?php endif ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>


		<?php endif; ?>
		<?php if(count($_POST['senator']) > 9): ?>
				<div class="container">
					<div class="row mt-5">
						<div class="col-md-12">
							<h5 class="text-danger alert alert-danger"><span class="fa fa-warning"></span> You've choose more than 8 senators! Please press back and review your selection</h5>
							<button type="button" class="btn btn-default" onclick="history.back();"><span class="fa fa-arrow-left"></span> Back</button>
						</div>
					</div>
				</div>
		<?php endif ?>

<?php else: ?>

	<?php include 'includes/403error.php'; ?>

<?php endif ?>

<?php include('includes/footer.php'); ?>