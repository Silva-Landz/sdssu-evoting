	<?php include('config.php'); ?>
	
	<?php if ($uri_segments[2] == '' || $uri_segments[2] == 'index.php'): ?>
		<!-- Optional JavaScript -->
	    <script src="assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<?php elseif ($uri_segments[2] == 'voting.php'): ?>
		<div class="container mt-5">
			<div class="col-md-12">
			<hr>
			<div class="row justify-content-center text-center">
				<p class="text-muted">SDSSU Automated Voting System. Copyright 2018<br/>
					All rights reserved
				</p>
				</div>
			</div>
		</div>
		<!-- Optional JavaScript -->
	    <script src="assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<?php elseif ($uri_segments[2] == 'review_ballot.php'): ?>
		<div class="container mt-5">
			<div class="col-md-12">
			<hr>
			<div class="row justify-content-center text-center">
				<p class="text-muted">SDSSU Automated Voting System. Copyright 2018<br/>
					All rights reserved
				</p>
				</div>
			</div>
		</div>
		<!-- Optional JavaScript -->
	    <script src="assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	    <script src="assets/js/sweetalert2.all.js" type="text/javascript"></script>
	    <script src="assets/js/custom.js" crossorigin="anonymous"></script>
	<?php elseif ($uri_segments[2] == 'view_profile.php'): ?>
		<div class="container mt-5">
			<div class="col-md-12">
			<hr>
			<div class="row justify-content-center text-center">
				<p class="text-muted">SDSSU Automated Voting System. Copyright 2018<br/>
					All rights reserved
				</p>
				</div>
			</div>
		</div>
		<!-- Optional JavaScript -->
	    <script src="assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="assets/js/bootstrap.min.js" crossorigin="anonymous"></script>           
	<?php elseif ($uri_segments[3] == '' || $uri_segments[3] == 'index.php'): ?>
		<!-- Optional JavaScript -->
	    <script src="../assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	    
    <?php elseif ($uri_segments[3] == 'dashboard.php'): ?>
    	<div class="container mt-5">
			<div class="col-md-12">
			<hr>
			<div class="row justify-content-center text-center">
				<p class="text-muted">SDSSU Automated Voting System. Copyright 2018<br/>
					All rights reserved
				</p>
				</div>
			</div>
		</div>
		<!-- Optional JavaScript -->
	    <script src="../assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/custom.js" crossorigin="anonymous"></script>
    <?php elseif ($uri_segments[3] == 'candidates_list.php'): ?>
    	<div class="container mt-5">
			<div class="col-md-12">
			<hr>
			<div class="row justify-content-center text-center">
				<p class="text-muted">SDSSU Automated Voting System. Copyright 2018<br/>
					All rights reserved
				</p>
				</div>
			</div>
		</div>
		<!-- Optional JavaScript -->
	    <script src="../assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
	    <script src="../assets/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
	    <script src="../assets/js/sweetalert2.all.js" type="text/javascript"></script>
	    <script src="../assets/js/custom.js" crossorigin="anonymous"></script>
	<?php elseif ($uri_segments[3] == 'voters_list.php'): ?>
		<div class="container mt-5">
			<div class="col-md-12">
			<hr>
			<div class="row justify-content-center text-center">
				<p class="text-muted">SDSSU Automated Voting System. Copyright 2018<br/>
					All rights reserved
				</p>
				</div>
			</div>
		</div>
		<!-- Optional JavaScript -->
	    <script src="../assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
	    <script src="../assets/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
	    <script src="../assets/js/sweetalert2.all.js" type="text/javascript"></script>
	    <script src="../assets/js/custom.js" crossorigin="anonymous"></script>
	<?php elseif ($uri_segments[3] == 'canvassing_report.php'): ?>
		<div class="container mt-5">
			<div class="col-md-12">
			<hr>
			<div class="row justify-content-center text-center">
				<p class="text-muted">SDSSU Automated Voting System. Copyright 2018<br/>
					All rights reserved
				</p>
				</div>
			</div>
		</div>
		<!-- Optional JavaScript -->
	    <script src="../assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
	    <script src="../assets/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
	    <script src="../assets/js/custom.js" crossorigin="anonymous"></script>
	<?php elseif ($uri_segments[3] == 'history_log.php'): ?>
		<div class="container mt-5">
			<div class="col-md-12">
			<hr>
			<div class="row justify-content-center text-center">
				<p class="text-muted">SDSSU Automated Voting System. Copyright 2018<br/>
					All rights reserved
				</p>
				</div>
			</div>
		</div>
		<!-- Optional JavaScript -->
	    <script src="../assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
	    <script src="../assets/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
	    <script src="../assets/js/custom.js" crossorigin="anonymous"></script>
	<?php elseif ($uri_segments[3] == 'about.php'): ?>
		<div class="container mt-5">
			<div class="col-md-12">
			<hr>
			<div class="row justify-content-center text-center">
				<p class="text-muted">SDSSU Automated Voting System. Copyright 2018<br/>
					All rights reserved
				</p>
				</div>
			</div>
		</div>
		<!-- Optional JavaScript -->
	    <script src="../assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	    <script src="../assets/js/custom.js" crossorigin="anonymous"></script>
	<?php elseif ($uri_segments[4] == 'edit_candidate.php'): ?>
		<div class="container mt-5">
			<div class="col-md-12">
			<hr>
			<div class="row justify-content-center text-center">
				<p class="text-muted">SDSSU Automated Voting System. Copyright 2018<br/>
					All rights reserved
				</p>
				</div>
			</div>
		</div>
		<!-- Optional JavaScript -->
	    <script src="../../assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="../../assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	    <script src="../../assets/js/custom.js" crossorigin="anonymous"></script>
	<?php elseif ($uri_segments[4] == 'edit_voter.php'): ?>
		<div class="container mt-5">
			<div class="col-md-12">
			<hr>
			<div class="row justify-content-center text-center">
				<p class="text-muted">SDSSU Automated Voting System. Copyright 2018<br/>
					All rights reserved
				</p>
				</div>
			</div>
		</div>
		<!-- Optional JavaScript -->
	    <script src="../../assets/js/jquery.js" crossorigin="anonymous"></script>
	    <script src="../../assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
	    <script src="../../assets/js/custom.js" crossorigin="anonymous"></script>     
	<?php endif; ?>
	
</body>
</html>