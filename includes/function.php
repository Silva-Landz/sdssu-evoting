<?php

	function getAllCandidates()	{

		include 'verify/include/global.php';

		$sql = "SELECT * FROM sdssu_candidates sc 
				INNER JOIN sdssu_degrees sd
					ON sc.deg_id = sd.deg_id
				INNER JOIN sdssu_positions sp 
					ON sc.pos_id = sp.pos_id";
		$result = $conn->query($sql);
		$arr = array();
		$i = 0;

		while ($rows = $result->fetch_assoc()) {
			$arr[$i] = array(
				'can_id' 	=> $rows['can_id'],
				'pos_id' 	=> $rows['pos_id'],
				'pos_type'  => $rows['pos_type'],
				'fname'	 	=> $rows['fname'],
				'lname' 	=> $rows['lname'],
				'mname' 	=> $rows['mname'],
				'gender' 	=> $rows['gender'],
				'deg_id' 	=> $rows['deg_id'],
				'deg_code' 	=> $rows['deg_code'],
				'deg_name' 	=> $rows['deg_name'],
				'year_lvl'	=> $rows['year_lvl'],
				'party' 	=> $rows['party'],
				'image' 	=> $rows['image']
			);
			$i++;
		}

		return $arr;
	}

	function getSpecificCan($can_id)	{
		
		include 'verify/include/global.php';

		$sql = "SELECT *  FROM sdssu_candidates sc
				INNER JOIN sdssu_degrees sd
					ON sc.deg_id = sd.deg_id
				INNER JOIN sdssu_positions sp 
					ON sc.pos_id = sp.pos_id
				WHERE sc.can_id = $can_id";

		$result = $conn->query($sql);
		$arr = array();
		$i = 0;

		while ($rows = $result->fetch_assoc()) {
			$arr[$i] = array(
				'can_id' 	=> $rows['can_id'],
				'pos_id' 	=> $rows['pos_id'],
				'pos_type'	=> $rows['pos_type'],
				'fname'	 	=> $rows['fname'],
				'lname' 	=> $rows['lname'],
				'mname' 	=> $rows['mname'],
				'gender' 	=> $rows['gender'],
				'deg_id' 	=> $rows['deg_id'],
				'deg_name' 	=> $rows['deg_name'],
				'year_lvl'	=> $rows['year_lvl'],
				'party' 	=> $rows['party'],
				'image' 	=> $rows['image']
			);
			$i++;
		}

		return $arr;
	}

	function getPresident()	{

		include 'verify/include/global.php';

		$sql = "SELECT * FROM sdssu_candidates 
				INNER JOIN sdssu_degrees 
					ON sdssu_candidates.deg_id = sdssu_degrees.deg_id
				INNER JOIN sdssu_positions 
					ON sdssu_candidates.pos_id = sdssu_positions.pos_id
				WHERE sdssu_candidates.pos_id = 1";

		$result = $conn->query($sql);
		$arr = array();
		$i = 0;

		while ($rows = $result->fetch_assoc()) {
			$arr[$i] = array(
				'can_id' 	=> $rows['can_id'],
				'pos_id' 	=> $rows['pos_id'],
				'fname'	 	=> $rows['fname'],
				'lname' 	=> $rows['lname'],
				'mname' 	=> $rows['mname'],
				'gender' 	=> $rows['gender'],
				'deg_id' 	=> $rows['deg_id'],
				'year_lvl'	=> $rows['year_lvl'],
				'party' 	=> $rows['party'],
				'image' 	=> $rows['image']
			);
			$i++;
		}

		return $arr;

	}

	function getViceInternal()	{

		include 'verify/include/global.php';

		$sql = "SELECT * FROM sdssu_candidates 
				INNER JOIN sdssu_degrees 
					ON sdssu_candidates.deg_id = sdssu_degrees.deg_id
				INNER JOIN sdssu_positions 
					ON sdssu_candidates.pos_id = sdssu_positions.pos_id 
				WHERE sdssu_candidates.pos_id = 2";

		$result = $conn->query($sql);
		$arr = array();
		$i = 0;

		while ($rows = $result->fetch_assoc()) {
			$arr[$i] = array(
				'can_id' 	=> $rows['can_id'],
				'pos_id' 	=> $rows['pos_id'],
				'fname'	 	=> $rows['fname'],
				'lname' 	=> $rows['lname'],
				'mname' 	=> $rows['mname'],
				'gender' 	=> $rows['gender'],
				'deg_id' 	=> $rows['deg_id'],
				'year_lvl'	=> $rows['year_lvl'],
				'party' 	=> $rows['party'],
				'image' 	=> $rows['image']
			);
			$i++;
		}

		return $arr;

	}

	function getViceExternal()	{

		include 'verify/include/global.php';

		$sql = "SELECT * FROM sdssu_candidates 
				INNER JOIN sdssu_degrees 
					ON sdssu_candidates.deg_id = sdssu_degrees.deg_id
				INNER JOIN sdssu_positions 
					ON sdssu_candidates.pos_id = sdssu_positions.pos_id
				WHERE sdssu_candidates.pos_id = 3";

		$result = $conn->query($sql);
		$arr = array();
		$i = 0;

		while ($rows = $result->fetch_assoc()) {
			$arr[$i] = array(
				'can_id' 	=> $rows['can_id'],
				'pos_id' 	=> $rows['pos_id'],
				'fname'	 	=> $rows['fname'],
				'lname' 	=> $rows['lname'],
				'mname' 	=> $rows['mname'],
				'gender' 	=> $rows['gender'],
				'deg_id' 	=> $rows['deg_id'],
				'year_lvl'	=> $rows['year_lvl'],
				'party' 	=> $rows['party'],
				'image' 	=> $rows['image']
			);
			$i++;
		}

		return $arr;

	}

	function getSecretary()	{

		include 'verify/include/global.php';

		$sql = "SELECT * FROM sdssu_candidates 
				INNER JOIN sdssu_degrees 
					ON sdssu_candidates.deg_id = sdssu_degrees.deg_id
				INNER JOIN sdssu_positions 
					ON sdssu_candidates.pos_id = sdssu_positions.pos_id
				WHERE sdssu_candidates.pos_id = 4";

		$result = $conn->query($sql);
		$arr = array();
		$i = 0;

		while ($rows = $result->fetch_assoc()) {
			$arr[$i] = array(
				'can_id' 	=> $rows['can_id'],
				'pos_id' 	=> $rows['pos_id'],
				'fname'	 	=> $rows['fname'],
				'lname' 	=> $rows['lname'],
				'mname' 	=> $rows['mname'],
				'gender' 	=> $rows['gender'],
				'deg_id' 	=> $rows['deg_id'],
				'year_lvl'	=> $rows['year_lvl'],
				'party' 	=> $rows['party'],
				'image' 	=> $rows['image']
			);
			$i++;
		}

		return $arr;

	}

	function getTreasurer()	{

		include 'verify/include/global.php';

		$sql = "SELECT * FROM sdssu_candidates 
				INNER JOIN sdssu_degrees 
					ON sdssu_candidates.deg_id = sdssu_degrees.deg_id
				INNER JOIN sdssu_positions 
					ON sdssu_candidates.pos_id = sdssu_positions.pos_id  
				WHERE sdssu_candidates.pos_id = 5";

		$result = $conn->query($sql);
		$arr = array();
		$i = 0;

		while ($rows = $result->fetch_assoc()) {
			$arr[$i] = array(
				'can_id' 	=> $rows['can_id'],
				'pos_id' 	=> $rows['pos_id'],
				'fname'	 	=> $rows['fname'],
				'lname' 	=> $rows['lname'],
				'mname' 	=> $rows['mname'],
				'gender' 	=> $rows['gender'],
				'deg_id' 	=> $rows['deg_id'],
				'year_lvl'	=> $rows['year_lvl'],
				'party' 	=> $rows['party'],
				'image' 	=> $rows['image']
			);
			$i++;
		}

		return $arr;

	}

	function getAuditor()	{

		include 'verify/include/global.php';

		$sql = "SELECT * FROM sdssu_candidates 
				INNER JOIN sdssu_degrees 
					ON sdssu_candidates.deg_id = sdssu_degrees.deg_id
				INNER JOIN sdssu_positions 
					ON sdssu_candidates.pos_id = sdssu_positions.pos_id  
				WHERE sdssu_candidates.pos_id = 6";

		$result = $conn->query($sql);
		$arr = array();
		$i = 0;

		while ($rows = $result->fetch_assoc()) {
			$arr[$i] = array(
				'can_id' 	=> $rows['can_id'],
				'pos_id' 	=> $rows['pos_id'],
				'fname'	 	=> $rows['fname'],
				'lname' 	=> $rows['lname'],
				'mname' 	=> $rows['mname'],
				'gender' 	=> $rows['gender'],
				'deg_id' 	=> $rows['deg_id'],
				'year_lvl'	=> $rows['year_lvl'],
				'party' 	=> $rows['party'],
				'image' 	=> $rows['image']
			);
			$i++;
		}

		return $arr;

	}

	function getSentor()	{

		include 'verify/include/global.php';

		$sql = "SELECT * FROM sdssu_candidates 
				INNER JOIN sdssu_degrees 
					ON sdssu_candidates.deg_id = sdssu_degrees.deg_id
				INNER JOIN sdssu_positions 
					ON sdssu_candidates.pos_id = sdssu_positions.pos_id  
				WHERE sdssu_candidates.pos_id = 7";

		$result = $conn->query($sql);
		$arr = array();
		$i = 0;

		while ($rows = $result->fetch_assoc()) {
			$arr[$i] = array(
				'can_id' 	=> $rows['can_id'],
				'pos_id' 	=> $rows['pos_id'],
				'fname'	 	=> $rows['fname'],
				'lname' 	=> $rows['lname'],
				'mname' 	=> $rows['mname'],
				'gender' 	=> $rows['gender'],
				'deg_id' 	=> $rows['deg_id'],
				'year_lvl'	=> $rows['year_lvl'],
				'party' 	=> $rows['party'],
				'image' 	=> $rows['image']
			);
			$i++;
		}

		return $arr;

	}

	function getVotes($can_id)	{

		include 'verify/include/global.php';

		$sql = "SELECT SUM(total_votes) sdssu_votes WHERE can_id='$can_id'";
		$result = $conn->query($sql);
		$ar = array();
		$i = 0;

		while ($rows = $result->fetch_assoc()) {
			$arr[$i] = array(
				'can_id' 		=> $rows['can_id'],
				'total_votes' 	=> $rows['total_votes']
			);
			$i++;
		}

		return $arr;
	}

	function countVoted()	{

		include '../verify/include/global.php';

		$sql = "SELECT * FROM sdssu_voters WHERE status=1";
		$result = $conn->query($sql);

		return $result->num_rows;
	}

	function countUnvoted()	{

		include '../verify/include/global.php';

		$sql = "SELECT * FROM sdssu_voters WHERE status=0";
		$result = $conn->query($sql);

		return $result->num_rows;

	}

	function countVaC($table)	{

		include '../verify/include/global.php';

		$sql = "SELECT * FROM $table";
		$result = $conn->query($sql);

		return $result->num_rows;
	}

	function countCandidateVotes($can_id)	{

		include '../verify/include/global.php';

		$sql = "SELECT * FROM sdssu_votes WHERE can_id='$can_id' AND total_votes=1";
		$result = $conn->query($sql);

		return $result->num_rows;
	}

	function getUserLog()	{

		include '../verify/include/global.php';

		$sql = "SELECT * FROM sdssu_log INNER JOIN sdssu_users ON sdssu_log.user_id=sdssu_users.user_id";
		$result = $conn->query($sql);
		$arr = array();
		$i = 0;

		while ($rows = $result->fetch_assoc()) {
			$arr[$i] = array(
				'user_id' 	=> $rows['user_id'],
				'fname'	 	=> $rows['fname'],
				'lname' 	=> $rows['lname'],
				'user_type'	=> $rows['user_type'],
				'log_id' 	=> $rows['log_id'],
				'action'	=> $rows['action'],
				'date' 		=> $rows['date']
			);
			$i++;
		}

		return $arr;
		
	}


	function insertHistoryLog($action)	{

		include '../../verify/include/global.php';

		$sql = "INSERT INTO sdssu_log (user_id, action) VALUES (1, '$action')";
		$result = $conn->query($sql);

		return $result;
	}
