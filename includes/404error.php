<div class="container">
	<div class="row mt-5 mb-5">
		<div class="col-md-12 mt-5 mb-5 text-center">
			<h1 class="display-1"><b>404</b></h1>
			<p class="lead text-muted">Page not found</p>
			<p>The page you are looking for doesn't exist or another error occured.</p>
			<a href="verify" class="btn btn-primary"><span class="fa fa-lock"></span> Login</a>		
		</div>
	</div>
</div>