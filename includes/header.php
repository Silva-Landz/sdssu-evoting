<?php include('config.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php if ($uri_segments[2] == '' || $uri_segments[2] == 'index.php'): ?>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
		<title>SDSSU Evote | Home</title>
		</head>
	<body style="background-color: #076794; color: white;">
	<?php elseif ($uri_segments[2] == 'voting.php'): ?>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
		<title>SDSSU Evote | Voting</title>
		</head>
	<body>
	<?php elseif ($uri_segments[2] == 'review_ballot.php'): ?>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
		<title>SDSSU Evote | Review Ballot</title>
		</head>
	<body>
	<?php elseif ($uri_segments[2] == 'view_profile.php'): ?>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
		<title>SDSSU Evote | View Profile</title>
		</head>
	<body>	
	<?php elseif ($uri_segments[2] == 'success.php'): ?>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
		<title>SDSSU Evote | Success</title>
		</head>
	<body>
	<?php elseif ($uri_segments[2] == 'failed.php'): ?>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
		<title>SDSSU Evote | Failed</title>
		</head>
	<body>		
	<?php elseif ($uri_segments[3] == '' || $uri_segments[3] == 'index.php'): ?>
		<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/custom.css">	
		<title>SDSSU Evote | Admin Login</title>
		</head>
	<body style="background-color: #076794; color: white;">
	<!-- Dashboard -->
	<?php elseif ($uri_segments[3] == 'dashboard.php'): ?>
		<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
		<title>SDSSU Evote | Dashboard</title>
		</head>
	<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	    <a class="navbar-brand" href="/sdssuevote">
	    	<img src="../assets/images/sdssulogo.png" width="50" height="45">
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
	        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
	            <li class="nav navbar-nav">
	                <a class="nav-link text-warning" href="#"><i class="fa fa-tachometer"></i> Dashboard <span class="sr-only">(current)</span></a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/candidates_list.php"><span class="fa fa-user"></span> Candidates </a>
	            </li>
	           <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/voters_list.php"><span class="fa fa-user"></span> Voters</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/canvassing_report.php"><span class="fa fa-book"></span> Canvassing Report</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/history_log.php"><span class="fa fa-history"></span> History Log</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/about.php"><span class="fa fa-info-circle"></span> About</a>
	            </li>
	        </ul>
	        <ul class="nav navbar-nav navbar-right">
	        	<div class="dropdown show">
			    <a class="btn btn-outline-light dropdown-toggle mr-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Administrator </a>

				    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
				        <a class="dropdown-item" href="../admin/action/logout.php"><span class="fa fa-sign-out"></span> Log Out</a>
				    </div>
				</div>
	        </ul>
	    </div>
	</nav>
	<!-- Candidates -->
	<?php elseif ($uri_segments[3] == 'candidates_list.php'): ?>
		<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
		<title>SDSSU Evote | Candidates</title>
		</head>
	<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	    <a class="navbar-brand" href="/sdssuevote">
	    	<img src="../assets/images/sdssulogo.png" width="50" height="45">
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
	        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/dashboard.php"><i class="fa fa-tachometer"></i> Dashboard <span class="sr-only">(current)</span></a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-warning" href="#"><span class="fa fa-user"></span> Candidates </a>
	            </li>
	           <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/voters_list.php"><span class="fa fa-user"></span> Voters</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/canvassing_report.php"><span class="fa fa-book"></span> Canvassing Report</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/history_log.php"><span class="fa fa-history"></span> History Log</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/about.php"><span class="fa fa-info-circle"></span> About</a>
	            </li>
	        </ul>
	        <ul class="nav navbar-nav navbar-right">
	        	<div class="dropdown show">
			    <a class="btn btn-outline-light dropdown-toggle mr-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Administrator </a>

				    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
				        <a class="dropdown-item" href="../admin/action/logout.php"><span class="fa fa-sign-out"></span> Log Out</a>
				    </div>
				</div>
	        </ul>
	    </div>
	</nav>
	<!-- Voters -->
	<?php elseif ($uri_segments[3] == 'voters_list.php'): ?>
		<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
		<title>SDSSU Evote | Voters</title>
		</head>
	<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	    <a class="navbar-brand" href="/sdssuevote">
	    	<img src="../assets/images/sdssulogo.png" width="50" height="45">
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
	        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/dashboard.php"><i class="fa fa-tachometer"></i> Dashboard <span class="sr-only">(current)</span></a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/candidates_list.php"><span class="fa fa-user"></span> Candidates </a>
	            </li>
	           <li class="nav navbar-nav">
	                <a class="nav-link text-warning active" href="#"><span class="fa fa-user"></span> Voters</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/canvassing_report.php"><span class="fa fa-book"></span> Canvassing Report</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/history_log.php"><span class="fa fa-history"></span> History Log</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/about.php"><span class="fa fa-info-circle"></span> About</a>
	            </li>
	        </ul>
	        <ul class="nav navbar-nav navbar-right">
	        	<div class="dropdown show">
			    <a class="btn btn-outline-light dropdown-toggle mr-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Administrator </a>

				    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
				        <a class="dropdown-item" href="../admin/action/logout.php"><span class="fa fa-sign-out"></span> Log Out</a>
				    </div>
				</div>
	        </ul>
	    </div>
	</nav>
	<!-- Canvassing report -->
	<?php elseif ($uri_segments[3] == 'canvassing_report.php'): ?>
		<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
		<title>SDSSU Evote | Canvassing Report</title>
		</head>
	<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	    <a class="navbar-brand" href="/sdssuevote">
	    	<img src="../assets/images/sdssulogo.png" width="50" height="45">
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
	        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/dashboard.php"><i class="fa fa-tachometer"></i> Dashboard <span class="sr-only">(current)</span></a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/candidates_list.php"><span class="fa fa-user"></span> Candidates </a>
	            </li>
	           <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/voters_list.php"><span class="fa fa-user"></span> Voters</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-warning active" href="#"><span class="fa fa-book"></span> Canvassing Report</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/history_log.php"><span class="fa fa-history"></span> History Log</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/about.php"><span class="fa fa-info-circle"></span> About</a>
	            </li>
	        </ul>
	        <ul class="nav navbar-nav navbar-right">
	        	<div class="dropdown show">
			    <a class="btn btn-outline-light dropdown-toggle mr-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Administrator </a>

				    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
				        <a class="dropdown-item" href="../admin/action/logout.php"><span class="fa fa-sign-out"></span> Log Out</a>
				    </div>
				</div>
	        </ul>
	    </div>
	</nav>
	<!-- History Log -->
	<?php elseif ($uri_segments[3] == 'history_log.php'): ?>
		<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
		<title>SDSSU Evote | History Log</title>
		</head>
	<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	    <a class="navbar-brand" href="/sdssuevote">
	    	<img src="../assets/images/sdssulogo.png" width="50" height="45">
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
	        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/dashboard.php"><i class="fa fa-tachometer"></i> Dashboard <span class="sr-only">(current)</span></a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/candidates_list.php"><span class="fa fa-user"></span> Candidates </a>
	            </li>
	           <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/voters_list.php"><span class="fa fa-user"></span> Voters</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/canvassing_report.php"><span class="fa fa-book"></span> Canvassing Report</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-warning active" href="#"><span class="fa fa-history"></span> History Log</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/about.php"><span class="fa fa-info-circle"></span> About</a>
	            </li>
	        </ul>
	        <ul class="nav navbar-nav navbar-right">
	        	<div class="dropdown show">
			    <a class="btn btn-outline-light dropdown-toggle mr-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Administrator </a>

				    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
				        <a class="dropdown-item" href="../admin/action/logout.php"><span class="fa fa-sign-out"></span> Log Out</a>
				    </div>
				</div>
	        </ul>
	    </div>
	</nav>
	<!-- History Log -->
	<?php elseif ($uri_segments[3] == 'about.php'): ?>
		<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
		<title>SDSSU Evote | About</title>
		</head>
	<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	    <a class="navbar-brand" href="/sdssuevote">
	    	<img src="../assets/images/sdssulogo.png" width="50" height="45">
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
	        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/dashboard.php"><i class="fa fa-tachometer"></i> Dashboard <span class="sr-only">(current)</span></a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/candidates_list.php"><span class="fa fa-user"></span> Candidates </a>
	            </li>
	           <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/voters_list.php"><span class="fa fa-user"></span> Voters</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/canvassing_report.php"><span class="fa fa-book"></span> Canvassing Report</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../admin/history_log.php"><span class="fa fa-history"></span> History Log</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-warning active" href="#"><span class="fa fa-info-circle"></span> About</a>
	            </li>
	        </ul>
	        <ul class="nav navbar-nav navbar-right">
	        	<div class="dropdown show">
			    <a class="btn btn-outline-light dropdown-toggle mr-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Administrator </a>

				    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
				        <a class="dropdown-item" href="../admin/action/logout.php"><span class="fa fa-sign-out"></span> Log Out</a>
				    </div>
				</div>
	        </ul>
	    </div>
	</nav>
	<?php elseif ($uri_segments[4] == 'edit_candidate.php'): ?>
		<link rel="stylesheet" type="text/css" href="../../assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../../assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../../assets/css/custom.css">
		<title>SDSSU Evote | Edit Candidate</title>
		</head>
	<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	    <a class="navbar-brand" href="/sdssuevote">
	    	<img src="../../assets/images/sdssulogo.png" width="50" height="45">
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
	        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../../admin/dashboard.php"><i class="fa fa-tachometer"></i> Dashboard <span class="sr-only">(current)</span></a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../../admin/candidates_list.php"><span class="fa fa-user"></span> Candidates </a>
	            </li>
	           <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../../admin/voters_list.php.php"><span class="fa fa-user"></span> Voters</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../../admin/canvassing_report.php"><span class="fa fa-book"></span> Canvassing Report</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../../admin/history_log.php"><span class="fa fa-history"></span> History Log</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-warning active" href="#"><span class="fa fa-info-circle"></span> About</a>
	            </li>
	        </ul>
	        <ul class="nav navbar-nav navbar-right">
	        	<div class="dropdown show">
			    <a class="btn btn-outline-light dropdown-toggle mr-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Administrator </a>

				    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
				        <a class="dropdown-item" href="../admin/action/logout.php"><span class="fa fa-sign-out"></span> Log Out</a>
				    </div>
				</div>
	        </ul>
	    </div>
	</nav>
	<?php elseif ($uri_segments[4] == 'edit_voter.php'): ?>
		<link rel="stylesheet" type="text/css" href="../../assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../../assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../../assets/css/custom.css">
		<title>SDSSU Evote | Edit Voter</title>
		</head>
	<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	    <a class="navbar-brand" href="/sdssuevote">
	    	<img src="../../assets/images/sdssulogo.png" width="50" height="45">
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
	        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../../admin/dashboard.php"><i class="fa fa-tachometer"></i> Dashboard <span class="sr-only">(current)</span></a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../../admin/candidates_list.php"><span class="fa fa-user"></span> Candidates </a>
	            </li>
	           <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../../admin/voters_list.php.php"><span class="fa fa-user"></span> Voters</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../../admin/canvassing_report.php"><span class="fa fa-book"></span> Canvassing Report</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-light" href="../../admin/history_log.php"><span class="fa fa-history"></span> History Log</a>
	            </li>
	            <li class="nav navbar-nav">
	                <a class="nav-link text-warning active" href="#"><span class="fa fa-info-circle"></span> About</a>
	            </li>
	        </ul>
	        <ul class="nav navbar-nav navbar-right">
	        	<div class="dropdown show">
			    <a class="btn btn-outline-light dropdown-toggle mr-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Administrator </a>

				    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
				        <a class="dropdown-item" href="#"><span class="fa fa-sign-out"></span> Log Out</a>
				    </div>
				</div>
	        </ul>
	    </div>
	</nav>
	<?php endif; ?>
