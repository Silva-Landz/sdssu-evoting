<?php 
	
	include 'verify/include/global.php';

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		session_start();

		$voters_id = $_POST['voters_id']; 

		$president = "";
		$viceInternal = "";
		$viceExternal = "";
		$secretary = "";
		$treasurer = "";
		$auditor = "";
		$senator = NULL;
		$count = 0;

		if (empty($_POST['president']) || $_POST['president'] == NULL) {
			$president = "";
			$count1 = 0;
		} else {
			$president = $_POST['president'];
			$count1 = 1;
		}

		if (empty($_POST['viceInternal']) || $_POST['viceInternal'] == NULL) {
			$viceInternal = "";
			$count2 = 0;
		} else {
			$viceInternal = $_POST['viceInternal'];
			$count2 = 1;
		} 

		if (empty($_POST['viceExternal']) || $_POST['viceExternal'] == NULL) {
			$viceExternal = "";
			$count3 = 0;
		} else {
			$viceExternal = $_POST['viceExternal'];
			$count3 = 1;
		}

		if (empty($_POST['secretary']) || $_POST['secretary'] == NULL) {
			$secretary = "";
			$count4 = 0;
		} else {
			$secretary = $_POST['secretary'];
			$count4 = 1;
		}

		if (empty($_POST['treasurer']) || $_POST['treasurer'] == NULL) {
			$treasurer = "";
			$count5 = 0;
		} else {
			$treasurer = $_POST['treasurer'];
			$count5 = 1;
		}

		if (empty($_POST['auditor']) || $_POST['auditor'] == NULL) {
			$auditor = "";
			$count6 = 0;
		} else {
			$auditor = $_POST['auditor'];
			$count6 = 1;
		}

		if (is_null($_POST['senator'])) {
			$senator = NULL;
			$count7 = 0;
		} else {
			$senator = $_POST['senator'];
			$count7 = 1;
		}
			
		$sql = "
				INSERT INTO 
					sdssu_votes (can_id, total_votes) 
				VALUES 
					('$president', '$count1'), ('$viceInternal', '$count2'), ('$viceExternal', '$count3'),
					('$secretary', '$count4'), ('$treasurer', '$count5'), ('$auditor', '$count6'), ";
		if (is_null($senator)) {
			$sql .= "('$senator', $count7)";
		} else {
			foreach ($senator as $sen)	{
			 	$sql .= " ('". $sen ."', '$count7'),";
			 } 
			$sql = rtrim($sql, ',');	
		}	

		if ($conn->query($sql) === TRUE) {
			$vSql = "UPDATE sdssu_voters SET status = 1 WHERE voters_id='".$voters_id."'";
			if ($conn->query($vSql)) {
				header('Location: success.php');
			}
		} else {
			header('Location: failed.php');
		}
		
	} else {
		echo "NOT WORKING";
	}