<?php 
	
	session_start();
	
	include '../verify/include/global.php';
	include '../includes/function.php';
	include '../includes/header.php'; 

?>

<?php if (!is_null($_SESSION['is_logged_in']) && isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] == 1): ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 bread-fluid">
			<nav aria-label="breadcrumb">
			    <ol class="breadcrumb">
			    	<li><b>You are currently here:</b> </li>
				    <li class="breadcrumb-item active" aria-current="page">&nbsp&nbspHistory Log</li>
			    </ol>
			</nav>
		</div>
	</div>
</div>

<div class="container">
	<div class="row mt-3 ml-1">
		<div class="col-md-12">
			<a href="#" class="btn btn-outline-success"><span class="fa fa-download"></span> Download</a>
		</div>
	</div>
	<div class="row mt-4">
		<div class="col-md-12">
			<table id="history_logs" class="table table-striped" style="width: 100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>User</th>
						<th>Action</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
				<?php $user_log = getUserLog(); ?>
				<?php if (count($user_log) > 0): ?>
					<?php foreach ($user_log as $log): ?>
						<tr>
							<td><?=$log['log_id']?></td>
							<td><?=$log['user_type']?></td>
							<td><?=$log['action']?></td>
							<td><?=$log['date']?></td>
						</tr>
					<?php endforeach ?>
				<?php endif ?>	
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php else: ?>
	<?php include '../includes/403error.php'; ?>
<?php endif; ?>

<?php include('../includes/footer.php'); ?>