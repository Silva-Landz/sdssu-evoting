<?php 
	
	session_start();	

	include '../includes/header.php'; 

?>

<?php if (!is_null($_SESSION['is_logged_in']) && isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] == 1): ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 bread-fluid">
			<nav aria-label="breadcrumb">
			    <ol class="breadcrumb">
			    	<li><b>You are currently here:</b> </li>
				    <li class="breadcrumb-item active" aria-current="page">&nbsp&nbspAbout</li>
			    </ol>
			</nav>
		</div>
	</div>
</div>

<div class="container">
	<div class="row mt-3 justify-content-center">
		<h1><b>Meet the team</b></h1>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="row mt-5">
				<div class="col-sm-6 text-center align-middle">
					<img src="../assets/images/user.png" class="rounded-circle img-responsive" width="200" height="200">
				</div>
				<div class="col-sm-6">
					<h5><b>Jonna C. Tabada</b></h5>
					<small class="text-muted blockquote-footer">Project Manager</small>
					<p class="mt-2">
						Graduated at Surigao del sur State University Cantilan campus. Currently working at Cantilan Bank Incorporated main branch.
					</p>
					<ul class="list-unstyled">
						<li>
							<small class="text-muted"><span class="fa fa-map-marker"></span> Cantilan, Surigao del sur</small>
						</li>
						<li>
							<small class="text-muted"><span class="fa fa-facebook-official"></span> facebook.com/thanyalee.tabada.1</small>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row mt-5">
				<div class="col-sm-6 text-center">
					<img src="../assets/images/user.png" class="rounded-circle" width="200" height="200">
				</div>
				<div class="col-sm-6">
					<h5><b>Sandy Boy M. Maloloy-on</b></h5>
					<small class="text-muted blockquote-footer">System's Analyst</small>
					<p class="mt-2">
						Graduated at Surigao del sur State University Cantilan campus. Currently working at Cantilan Bank Incorporated Madrid branch.
					</p>
					<ul class="list-unstyled">
						<li>
							<small class="text-muted"><span class="fa fa-map-marker"></span> Madrid, Surigao del sur</small>
						</li>
						<li>
							<small class="text-muted"><span class="fa fa-facebook-official"></span> facebook.com/im.seandee</small>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="row mt-5">
				<div class="col-sm-6 text-center align-middle">
					<img src="../assets/images/user.png" class="rounded-circle" width="200" height="200">
				</div>
				<div class="col-sm-6">
					<h5><b>Manuel V. Anib Jr.</b></h5>
					<small class="text-muted blockquote-footer">System's Analyst</small>
					<p class="mt-2">
						Graduated at Surigao del sur State University Cantilan campus. Currently working at Surigao del sur State University Cantilan campus.
					</p>
					<ul class="list-unstyled">
						<li>
							<small class="text-muted"><span class="fa fa-map-marker"></span> Cantilan, Surigao del sur</small>
						</li>
						<li>
							<small class="text-muted"><span class="fa fa-facebook-official"></span> facebook.com/manuel.anib</small>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row mt-5">
				<div class="col-sm-6 text-center">
					<img src="../assets/images/user.png" class="rounded-circle" width="200" height="200">
				</div>
				<div class="col-sm-6">
					<h5><b>Landrex O. Rebuera</b></h5>
					<small class="text-muted blockquote-footer">Programmer</small>
					<p class="mt-2">
						Graduated at Visayas State University, Baybay City, Leyte. Kaon, tulog, program ug dula ra ang trabaho ani.
					</p>
					<ul class="list-unstyled">
						<li>
							<small class="text-muted"><span class="fa fa-map-marker"></span> Madrid, Surigao del sur</small>
						</li>
						<li>
							<small class="text-muted"><span class="fa fa-facebook-official"></span> facebook.com/landrex.rebuera</small>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php else: ?>
	<?php include '../includes/403error.php'; ?>
<?php endif; ?>

<?php include('../includes/footer.php'); ?>