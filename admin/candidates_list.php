<?php 
	session_start();

	include '../verify/include/global.php';
	
	$sql_all  = "
				SELECT * FROM sdssu_candidates 
					INNER JOIN sdssu_positions 
						ON sdssu_candidates.pos_id = sdssu_positions.pos_id 
					INNER JOIN sdssu_degrees 
						ON sdssu_candidates.deg_id = sdssu_degrees.deg_id ORDER BY sdssu_candidates.pos_id ASC;						
				";
	$result0  = $conn->query($sql_all); 

	$sql_deg  = "SELECT * FROM sdssu_degrees"; 
	$result1  = $conn->query($sql_deg);

	$sql_pos  = "SELECT * FROM sdssu_positions";
	$result2  = $conn->query($sql_pos);

?>

<?php include('../includes/header.php'); ?>

<?php if (!is_null($_SESSION['is_logged_in']) && isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] == 1): ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 bread-fluid">
			<nav aria-label="breadcrumb">
			    <ol class="breadcrumb">
			    	<li><b>You are currently here:</b></li>
				    <li class="breadcrumb-item active" aria-current="page">&nbsp&nbspCandidates List</li>
			    </ol>
			</nav>
		</div>
	</div>
</div>

<div class="container">
	<div class="row mt-1">
		<div class="col-md-12">
			<?php if (empty($_SESSION['success'])): ?>
				<div></div>
			<?php elseif($_SESSION['success'] == 'YES'): ?>
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					<?php echo $_SESSION['message']; ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				 		<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php elseif($_SESSION['success'] == 'NO'): ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<?php echo $_SESSION['message']; ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				 		<span aria-hidden="true">&times;</span>
					</button>
				</div>

			<?php endif ?>
		</div>
	</div>
	<div class="row mt-1">
		<div class="col-md-6">
			<!-- Add modal for creating a new candidate -->
			<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addCandidate"><span class="fa fa-plus-circle"></span> New candidate</a>
			<!-- Modal -->
			<div class="modal fade" id="addCandidate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			    <div class="modal-dialog modal-lg" role="document">
			        <div class="modal-content">
			            <div class="modal-header">
			                <h5 class="modal-title" id="exampleModalLabel">Add a new candidate</h5>
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                    <span aria-hidden="true">&times;</span>
			                </button>
			            </div>
			            <div class="modal-body">
			            	<!-- Content here -->
			                <div class="container">
			                	<!-- <div class="row"> -->
			                		<div class="col-md-12">
			                		<!-- Creating a new candidate form -->
			                		<form method="POST" action="action/add_candidate.php" enctype="multipart/form-data">
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>First Name:</label>
			                				</div>
										    <div class="col-sm-9">
										    	<input type="text" class="form-control" id="exampleInputEmail1" name="fname" placeholder="First name" required="required">
										    </div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Last Name:</label>
			                				</div>
										    <div class="col-sm-9">
										    	<input type="text" class="form-control" id="exampleInputEmail1" name="lname" placeholder="Last name" required="required">
										    </div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Middle Name:</label>
			                				</div>
										    <div class="col-sm-9">
										    	<input type="text" class="form-control" id="exampleInputEmail1" name="mname" placeholder="Middle name" required="required">
										    </div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Gender:</label>
			                				</div>
			                				<div class="col-sm-9">
			                					<select class="form-control" name="gender" required="required">
			                					<option>Choose gender . . .</option>
			                					<option>Male</option>
			                					<option>Female</option>
			                				</select>
			                				</div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Degree Program:</label>
			                				</div>
			                				<div class="col-sm-9">
			                					<select class="form-control" name="degree" required="required">
			                					<option value="">Choose Degree Program . . .</option>
			                					<!-- fetch data from database -->
			                					<?php if($result1->num_rows > 0): ?>
													<?php while($row = $result1->fetch_assoc()): ?>
														<option value="<?=$row['deg_id']?>"><?=$row['deg_name']?></option>	
													<?php endwhile; ?>	
												<?php endif; ?>
			                				</select>
			                				</div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Year Level:</label>
			                				</div>
			                				<div class="col-sm-9">
			                					<select class="form-control" name="yearlvl" required="required">
				                					<option value="">Choose year level . . .</option>
				                					<option value="1st">1st year</option>
				                					<option value="2nd">2nd year</option>
				                					<option value="3rd">3rd year</option>
				                					<option value="4th">4th year</option>
				                					<option value="5th">5th year</option>
			                					</select>
			                				</div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Position:</label>
			                				</div>
			                				<div class="col-sm-9">
			                					<select class="form-control" name="pos" required="required">
				                					<option>Choose position . . .</option>
				                					<?php if($result2->num_rows > 0): ?>
														<?php while($row = $result2->fetch_assoc()): ?>
															<option value="<?=$row['pos_id']?>"><?=$row['pos_type']?></option>	
														<?php endwhile; ?>	
													<?php endif; ?>
			                					</select>
			                				</div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Party:</label>
			                				</div>
										    <div class="col-sm-9">
										    	<select class="form-control" name="party" required="required">
				                					<option value="">Choose party . . .</option>
				                					<option value="WISELY">WISELY</option>
				                					<option value="CHANGES">CHANGES</option>
			                					</select>
										    </div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Upload image:</label>
			                				</div>
										    <div class="col-sm-8 custom-file">
										    	<input type="file" name="upload_img" required='required' accept="image/x-png, image/gif, image/jpeg">
										    </div>
			                			</div>
			                	</div>
			                	<!-- </div> -->
			                </div>
			            </div>
			            <div class="modal-footer">
			                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			                <button type="submit" name="submit" class="btn btn-primary">Save changes</button>
			            </div>
			            </form>
			        </div>
			    </div>
			</div>
		</div>
		<div class="col-md-6 text-right">
			<form method="POST" action="action/download_candidates.php">
				<button type="submit" class="btn btn-outline-success"><span class="fa fa-download"></span> Download</button>
			</form>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col-md-12">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
			    <li class="nav-item">
			        <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">All candidates</a>
			    </li>
			</ul>
			<div class="tab-content mt-4" id="myTabContent">
			    <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
					<table id="all_candidates" class="table table-striped" style="width:100%">
				        <thead>
				            <tr>
				            	<th>#</th>
				            	<th>Photo</th>
				                <th>Position</th>
				                <th>Fullname</th>
				                <th>Party</th>
				                <th>Gender</th>
				                <th>Course & Year</th>
				                <th></th>
				                <th></th>
				            </tr>
				        </thead>
				        <tbody>
			            	<?php if($result0->num_rows != 0): ?>
								<?php 
									$i = 1;
									while($row = $result0->fetch_assoc()): 
								?>
										<tr>
											<td><?= $i++; ?></td>
											<td class="text-center">
												<?php 	
													if(empty($row['image']))	{
														$row['image'] = "../uploads/no-image.png";
														echo "<img src='".$row['image']."' width='40' height='40'>";
													} else {
														$image_path = "../uploads/".$row['image'];
														echo "<img src='".$image_path."' width='40' height='40'>";
													}
												?>
											</td>
											<td><?= $row['pos_type'] ?></td>
											<td><?= $row['fname']." ".$row['mname']." ".$row['lname'] ?></td>
											<td><?= $row['party'] ?></td>
											<td><?= $row['gender'] ?></td>
											<td><?= $row['deg_code']." - ".$row['year_lvl']." year"; ?></td>
											<td>
												<a href="action/edit_candidate.php?can_id=<?=$row['can_id']?>" class="btn btn-info"><span class="fa fa-pencil"></span></a>
											</td>
											<td>
												<a href="javascript:void(0)" class="btn btn-danger" name="delete_candidate" id="delete_candidate" data-id="<?=$row['can_id']?>"><span class="fa fa-trash"></span></a>
											</td>
										</tr>

								<?php endwhile; ?>	
							<?php endif; ?>
				        </tbody>
				    </table>

					<!-- add datatables -->
			    </div>
			</div>
		</div>
	</div>
</div>

<?php else: ?>
	<?php include '../includes/403error.php'; ?>
<?php endif; ?>

<?php 
	include('../includes/footer.php'); 
	unset($_SESSION['success']);
?>