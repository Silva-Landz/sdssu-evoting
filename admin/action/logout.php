<?php

	include '../../verify/include/global.php';
	include '../../includes/function.php';

	session_start();

	$actionPerformed = "Logged out";
	insertHistoryLog($actionPerformed);

	// sets session to zero
	$_SESSION['is_logged_in'] = 0;

	header('Location: ../');
	exit();