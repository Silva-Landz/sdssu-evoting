<?php

	include '../../verify/include/global.php';
	include '../../includes/function.php';

 	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update']))	{
 		
 		session_start();

 		$_SESSION['success'] = "";
 		$action_performed = 'Updating a voter';

 		$voter_id	  = $_POST['voter_id'];
 		$fname 		  = $_POST['fname'];
 		$lname		  = $_POST['lname'];
 		$mname		  = $_POST['mname'];
 		$gender		  = $_POST['gender'];
 		$degree		  = $_POST['degree_prog'];
 		$yearlvl	  = $_POST['yearlvl'];

 		$sql = "
 			UPDATE 
 				`sdssu_voters` 
 			SET 
 				`voters_id` = '".$voter_id."', `fname` ='".$fname."', 
 				`lname` ='".$lname."', `mname` ='".$mname."', 
 				`gender` ='".$gender."', `deg_id` ='".$degree."', 
 				`year_lvl` ='".$yearlvl."'
 			WHERE 
 				`voters_id` = '".$voter_id."';";

 		// var_dump($sql); die();

 		if ($conn->query($sql) === TRUE) {
 			// INSERT HISTORY LOGS here
			insertHistoryLog($actionPerfomed);
			
 			$_SESSION['success'] = "YES";
 			header("Location: ../voters_list.php");
 		} else {
 			$_SESSION['success'] = "NO";
 			header('Location: ../voters_list.php');
 		}

 		$conn->close();
 	} else {
 		echo "NOT WORKING...";

 	}