<?php 

	include '../../verify/include/global.php';


 	if($_SERVER['REQUEST_METHOD'] == 'POST')	{

 		$sql = "SELECT * FROM sdssu_candidates sc INNER JOIN sdssu_degrees sd ON sc.deg_id = sd.deg_id INNER JOIN sdssu_positions sp 
				ON sc.pos_id = sp.pos_id ORDER BY sc.pos_id ASC";

		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			$output = '
				<table class="table">
					<tr>
						<th class="text-center">ID</th>
				        <th class="text-center">Position</th>
				        <th class="text-center">Fullname</th>
				        <th class="text-center">Party</th>
				        <th class="text-center">Gender</th>
				        <th class="text-center">Course & Year</th>
				        <th class="text-center">Total Votes</th>
					</tr>
			';
			while ($rows = $result->fetch_assoc()) {
				$sqlVotes = "SELECT * FROM sdssu_votes WHERE can_id='".$rows['can_id']."' AND total_votes=1";
				$resVotes = $conn->query($sqlVotes);
				$numRows = $resVotes->num_rows;
				$output .= '
						<tr>
							<td>'.$rows["can_id"].'</td>
							<td>'.$rows["pos_type"].'</td>
							<td>'.$rows["fname"].' '.$rows["mname"].' '.$rows["lname"].'</td>
							<td>'.$rows["party"].'</td>
							<td>'.$rows["gender"].'</td>
							<td>'.$rows["deg_name"].' - '.$rows["year_lvl"].'</td>
							<td>'.$numRows.'</td>
						</tr>';
			}

			$output .='</table>';
			header("Content-Type: application/xls");
			header("Content-Disposition:attachment; filename=canvassing_report.xls");
			echo $output;
		}
 	}


