<?php

	include '../../verify/include/global.php';
	include '../../includes/function.php';

 	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update']))	{
 		
 		session_start();

 		$_SESSION['success'] = "";
 		$actionPerfomed = 'Updated a candidate';

 		$can_id		  = $_POST['can_id'];
 		$pos 		  = $_POST['pos'];
 		$fname 		  = $_POST['fname'];
 		$lname		  = $_POST['lname'];
 		$mname		  = $_POST['mname'];
 		$gender		  = $_POST['gender'];
 		$degree		  = $_POST['degree_prog'];
 		$yearlvl	  = $_POST['yearlvl'];
 		$party	 	  = $_POST['party'];

 		// Image upload
 		// $target_dir = "../../uploads/";
 		// $target_file = $target_dir . basename($_FILES['update_img']['name']);
 		// $uploaded_img = $_FILES['update_img']['name'];

 		// var_dump($uploaded_img); die();

 		$sql = "
 			UPDATE 
 				`sdssu_candidates` 
 			SET 
 				`pos_id` = '".$pos."', `fname` ='".$fname."', 
 				`lname` ='".$lname."', `mname` ='".$mname."', 
 				`gender` ='".$gender."', `deg_id` ='".$degree."', 
 				`year_lvl` ='".$yearlvl."',  `party` ='".$party."'
 			WHERE 
 				`can_id` = '".$can_id."';";


		if ($conn->query($sql) === TRUE) {
 			// INSERT HISTORY LOGS here
			insertHistoryLog($actionPerfomed);

 			$_SESSION['success'] = "YES";
 			$_SESSION['message'] = "<b>Successfully!</b> Updated ".$fname."'s' information.";
 			header("Location: ../candidates_list.php");
 		} else {
 			$_SESSION['success'] = "NO";
 			$_SESSION['message'] = "There was a problem updating the candidate.";
 			header('Location: ../candidates_list.php');
 		}

 		$conn->close();
 	} else {
 		echo "NOT WORKING...";

 	}