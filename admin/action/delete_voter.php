<?php

	include '../../verify/include/global.php';
	include '../../includes/function.php';

	$response = array();
	$actionPerfomed = "Deleted a voter";

	if ($_POST['delete_voter']) {
		
		$pid = intval($_POST['delete_voter']);

		$query = "DELETE sv,du,df FROM sdssu_voters sv 
				  INNER JOIN demo_user du 
				  	ON sv.voters_id = du.user_id 
				  INNER JOIN demo_finger df 
				  	ON du.user_id = df.user_id 
				  WHERE sv.voters_id='".$pid."'";

		if ($conn->query($query) === TRUE) {
			// insert history log
			insertHistoryLog($actionPerfomed);

			$response['status'] = 'success';
			$response['message'] = 'Voter Deleted Successfully';
		} else {
			$response['status'] = 'error';
			$response['message'] = 'Cannot delete candidate';
		}
		echo json_encode($response);

	}