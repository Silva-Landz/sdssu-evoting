<?php

	include '../../verify/include/global.php';
	include '../../includes/function.php';

	$actionPerfomed = "Deleted a candidate";
	$response = array();

	if ($_POST['delete_candidate']) {
		
		$pid = intval($_POST['delete_candidate']);

		$sql = "SELECT * FROM sdssu_candidates WHERE can_id='".$pid."'";
		$result 	 = $conn->query($sql);
		$row 		 = $result->fetch_assoc();
		$img_name 	 = $row['image'];
		$target_dir	 = "../../uploads/";
		$target_file = $target_dir . $img_name;

		if (file_exists($target_file)) {
			unlink($target_file);
		}

		$query = "DELETE FROM sdssu_candidates WHERE can_id='".$pid."'";

		if ($conn->query($query) === TRUE) {
			// insert history log
			insertHistoryLog($actionPerfomed);

			$response['status'] = 'success';
			$response['message'] = 'Candidate Deleted Successfully';
		} else {
			$response['status'] = 'error';
			$response['message'] = 'Cannot delete candidate';
		}
		echo json_encode($response);

	}