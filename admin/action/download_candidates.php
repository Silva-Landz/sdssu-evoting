<?php 

	include '../../verify/include/global.php';


 	if($_SERVER['REQUEST_METHOD'] == 'POST')	{

 		$sql =  "
				SELECT * FROM sdssu_candidates 
					INNER JOIN sdssu_positions 
						ON sdssu_candidates.pos_id = sdssu_positions.pos_id 
					INNER JOIN sdssu_degrees 
						ON sdssu_candidates.deg_id = sdssu_degrees.deg_id ORDER BY sdssu_candidates.pos_id ASC;						
				";


		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			$output = '
				<table class="table">
					<tr>
						<th class="text-center">ID</th>
				        <th class="text-center">Firstname</th>
				        <th class="text-center">Lastname</th>
				        <th class="text-center">M.I</th>
				        <th class="text-center">Position</th>
				        <th class="text-center">Party</th>
				        <th class="text-center">Gender</th>
				        <th class="text-center">Course</th>
				        <th class="text-center">Year</th>
					</tr>
			';
			while ($rows = $result->fetch_assoc()) {
				$output .= '
						<tr>
							<td>'.$rows["can_id"].'</td>
							<td>'.$rows["fname"].'</td>
							<td>'.$rows["lname"].'</td>
							<td>'.$rows["mname"].'</td>
							<td>'.$rows["pos_type"].'</td>
							<td>'.$rows["party"].'</td>
							<td>'.$rows["gender"].'</td>
							<td>'.$rows["deg_name"].'</td>
							<td>'.$rows["year_lvl"].'</td>
						</tr>';
			}

			$output .='</table>';
			header("Content-Type: application/xls");
			header("Content-Disposition:attachment; filename=candidates_list.xls");
			echo $output;
		}
 	}


