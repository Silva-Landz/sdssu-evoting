<?php 
	session_start();

	include '../../verify/include/global.php';

	$sql  = "
			SELECT * FROM sdssu_voters 
			INNER JOIN sdssu_degrees ON sdssu_voters.deg_id = sdssu_degrees.deg_id 
			WHERE sdssu_voters.voters_id = '".$_GET['voter_id']."'
			";
	
	$result  = $conn->query($sql);

	$sql_deg  = "SELECT * FROM sdssu_degrees"; 
	$result1  = $conn->query($sql_deg);
	
	include('../../includes/header.php');
?>
	
	<div class="container">
		<div class="row mt-3 justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">
						Edit voter
					</div>
					<div class="card-body">
						<form method="POST" action="update_voter.php" enctype="multipart/form-data">
							<?php if($result->num_rows != 0): ?>
								<?php while($rows = $result->fetch_assoc()): ?>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Firstname:</label>
											</div>
											<div class="col-md-9">
												<input type="hidden" name="voter_id" value="<?=$rows['voters_id']?>">
												<input type="text" name="fname" class="form-control" value="<?=$rows['fname']?>">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Lastname:</label>
											</div>
											<div class="col-md-9">
												<input type="text" name="lname" class="form-control" value="<?=$rows['lname']?>">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Middlename:</label>
											</div>
											<div class="col-md-9">
												<input type="text" name="mname" class="form-control" value="<?=$rows['mname']?>">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Gender:</label>
											</div>
											<div class="col-md-9">
												<select class="form-control" name="gender" required="required">
													<option>Choose gender . . .</option>
													<?php if ($rows['gender'] == 'Male'): ?>
						                					<option value="Male" selected='selected'>Male</option>
				                							<option value="Female">Female</option>
				                					<?php elseif($rows['gender'] == 'Female'): ?>
				                							<option value="Male">Male</option>
					                						<option value="Female" selected='selected'>Female</option>
													<?php endif ?>
				                				</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Degree Program:</label>
											</div>
											<div class="col-md-9">
												<select class="form-control" name="degree_prog" required="required">
				                					<option value="">Choose Degree Program . . .</option>
				                					<?php if($result1->num_rows > 0): ?>
				                						<?php while($rows1 = $result1->fetch_assoc()): ?>
				                							<?php if($rows1['deg_id'] == $rows['deg_id']): ?>
				                									<option value="<?=$rows['deg_id']?>" selected='selected'><?=$rows['deg_name']?></option>
															<?php else: ?>
																	<option value="<?=$rows1['deg_id']?>"><?=$rows1['deg_name']?></option>
				                							<?php endif; ?>
				                						<?php endwhile; ?>	
				                					<?php endif; ?>
				                				</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Year Level:</label>
											</div>
											<div class="col-md-9">
												<select class="form-control" name="yearlvl" required="required">
				                					<option value="">Choose Year Level . . .</option>
				                					<?php if ($rows['year_lvl'] == '1st'): ?>
														<option value="1st" selected='selected'>1st year</option>
														<option value="2nd">2nd year</option>
					                					<option value="3rd">3rd year</option>
					                					<option value="4th">4th year</option>
					                					<option value="5th">5th year</option>
	                								<?php elseif($rows['year_lvl'] == '2nd'): ?>
	                									<option value="1st">1st year</option>
	                									<option value="2nd" selected='selected'>2nd year</option>
	                									<option value="3rd">3rd year</option>
					                					<option value="4th">4th year</option>
					                					<option value="5th">5th year</option>
                									<?php elseif($rows['year_lvl'] == '3rd'): ?>
	                									<option value="1st">1st year</option>
	                									<option value="2nd">2nd year</option>
                										<option value="3rd" selected='selected'>3rd year</option>
                										<option value="4th">4th year</option>
					                					<option value="5th">5th year</option>
            										<?php elseif($rows['year_lvl'] == '4th'): ?>
            											<option value="1st">1st year</option>
	                									<option value="2nd">2nd year</option>
                										<option value="3rd">3rd year</option>
            											<option value="4th" selected='selected'>4th year</option>
            											<option value="5th">5th year</option>
        											<?php elseif($rows['year_lvl'] == '5th'): ?>
        												<option value="1st">1st year</option>
	                									<option value="2nd">2nd year</option>
                										<option value="3rd">3rd year</option>
            											<option value="4th">4th year</option>
        												<option value="5th" selected='selected'>5th year</option>
				                					<?php endif ?>
				                				</select>
											</div>
										</div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
					</div>
					<div class="card-footer text-right">
						<a href="../voters_list.php" class="btn btn-dark">Cancel</a>
						<button type="submit" name="update" class="btn btn-primary">Update changes</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include('../../includes/footer.php'); ?>