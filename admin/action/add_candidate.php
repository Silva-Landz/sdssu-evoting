<?php 
	include '../../verify/include/global.php';
	include '../../includes/function.php';

 	if($_SERVER['REQUEST_METHOD'] == 'POST')	{
 		
 		session_start();

 		$_SESSION['success'] = "";
 		$actionPerfomed = 'Inserted a new candidate';

 		$pos 		  = $_POST['pos'];
 		$fname 		  = $_POST['fname'];
 		$lname		  = $_POST['lname'];
 		$mname		  = $_POST['mname'];
 		$gender		  = $_POST['gender'];
 		$degree		  = $_POST['degree'];
 		$yearlvl	  = $_POST['yearlvl'];
 		$party	 	  = $_POST['party'];

 		// Image upload
 		$target_dir = "../../uploads/";
 		$target_file = $target_dir . basename($_FILES['upload_img']['name']);
 		$uploaded_img = $_FILES['upload_img']['name'];

 		$sql = "
 				INSERT INTO sdssu_candidates (pos_id, fname, lname, mname, gender, deg_id, year_lvl, party, image) 
 			   	VALUES ('".$pos."','".$fname."','".$lname."','".$mname."','".$gender."','".$degree."','".$yearlvl."','".$party."','".$uploaded_img."');
 			   ";

 		if (move_uploaded_file($_FILES['upload_img']['tmp_name'], $target_file)) {
 			if ($conn->query($sql) === TRUE) {
 				insertHistoryLog($actionPerfomed);
	 			// INSERT HISTORY LOGS here
	 			$_SESSION['success'] = "YES";
	 			$_SESSION['message'] = "<b>Successfully!</b> Inserted a new candidate.";
	 			header("Location: ../candidates_list.php");
	 		} else {
	 			$_SESSION['success'] = "NO";
	 			$_SESSION['message'] = "Unabe to insert a new candidate.";
	 			header('Location: ../candidates_list.php');
 			}

 		} else {
 			$_SESSION['success'] = "NO";
 			$_SESSION['message'] = "There was a problem uploading the image.";
 			header('Location: ../candidates_list.php');
 		}
 	} else {	echo "NOT WORKING..."; }
 	
 	$conn->close();