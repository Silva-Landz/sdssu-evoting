<?php 
	
	include '../../verify/include/global.php';

	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update']))	{

		session_start();

		$action_performed = "Updates candidate's photo";
		$can_id = $_POST['can_id'];
		$img_name = $_POST['img_name'];

		// Image upload
		$target_dir		 = "../../uploads/";
		$target_file 	 = $target_dir . basename($_FILES['update_img']['name']);
		$new_target_file = $target_dir . $img_name;
		$uploaded_img 	 = $_FILES['update_img']['name'];

		// Deletes the current image in the database before updating
		// it with new image.
		if(file_exists($new_target_file)) unlink($new_target_file);
	
		$sql = "
 			UPDATE 
 				`sdssu_candidates` 
 			SET 
 				`image` = '".$uploaded_img."'
 			WHERE 
 				`can_id` = '".$can_id."';";

 		if ($conn->query($sql) === TRUE) {
 			if (move_uploaded_file($_FILES['update_img']['tmp_name'], $target_file)) {
				$_SESSION['success'] = "YES";
	 			$_SESSION['message'] = "<b>Successfully updated candidate's photo</b>";
	 			header('Location: ../candidates_list.php');
			} else {
				$_SESSION['success'] = "NO";
	 			$_SESSION['message'] = "Can't update candidate's photo";
	 			header('Location: ../candidates_list.php');
			}
 		} else {
 			$_SESSION['success'] = "NO";
 			$_SESSION['message'] = "Something went wrong";
 			header('Location: ../candidates_list.php');
 		}
	} else {
		echo "NOT WORKING";
	}

	