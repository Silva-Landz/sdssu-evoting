<?php 
	session_start();

	include '../../verify/include/global.php';

	$sql  = "
			SELECT * FROM sdssu_candidates 
			INNER JOIN sdssu_positions ON sdssu_candidates.pos_id = sdssu_positions.pos_id 
			INNER JOIN sdssu_degrees ON sdssu_candidates.deg_id = sdssu_degrees.deg_id 
			WHERE sdssu_candidates.can_id = '".$_GET['can_id']."'
			";
	
	$result  = $conn->query($sql);

	$sql_deg  = "SELECT * FROM sdssu_degrees"; 
	$result1  = $conn->query($sql_deg);

	$sql_pos  = "SELECT * FROM sdssu_positions";
	$result2  = $conn->query($sql_pos);
	
	include('../../includes/header.php');
?>
	
	<div class="container">
		<div class="row mt-3 justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">
						Edit candidate's information
					</div>
					<div class="card-body">
						<form method="POST" action="update_candidate.php">
							<?php if($result->num_rows != 0): ?>
								<?php while($rows = $result->fetch_assoc()): ?>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Firstname:</label>
											</div>
											<div class="col-md-9">
												<input type="hidden" name="can_id" value="<?=$rows['can_id']?>">
												<input type="text" name="fname" class="form-control" value="<?=$rows['fname']?>">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Lastname:</label>
											</div>
											<div class="col-md-9">
												<input type="text" name="lname" class="form-control" value="<?=$rows['lname']?>">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Middlename:</label>
											</div>
											<div class="col-md-9">
												<input type="text" name="mname" class="form-control" value="<?=$rows['mname']?>">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Gender:</label>
											</div>
											<div class="col-md-9">
												<select class="form-control" name="gender" required="required">
													<option>Choose gender . . .</option>
													<?php if ($rows['gender'] == 'Male'): ?>
						                					<option value="Male" selected='selected'>Male</option>
				                							<option value="Female">Female</option>
				                					<?php elseif($rows['gender'] == 'Female'): ?>
				                							<option value="Male">Male</option>
					                						<option value="Female" selected='selected'>Female</option>
													<?php endif ?>
				                				</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Degree Program:</label>
											</div>
											<div class="col-md-9">
												<select class="form-control" name="degree_prog" required="required">
				                					<option value="">Choose Degree Program . . .</option>
				                					<?php if($result1->num_rows > 0): ?>
				                						<?php while($rows1 = $result1->fetch_assoc()): ?>
				                							<?php if($rows1['deg_id'] == $rows['deg_id']): ?>
				                									<option value="<?=$rows['deg_id']?>" selected='selected'><?=$rows['deg_name']?></option>
															<?php else: ?>
																	<option value="<?=$rows1['deg_id']?>"><?=$rows1['deg_name']?></option>
				                							<?php endif; ?>
				                						<?php endwhile; ?>	
				                					<?php endif; ?>
				                				</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Year Level:</label>
											</div>
											<div class="col-md-9">
												<select class="form-control" name="yearlvl" required="required">
				                					<option value="">Choose Year Level . . .</option>
				                					<?php if ($rows['year_lvl'] == '1st'): ?>
														<option value="1st" selected='selected'>1st year</option>
														<option value="2nd">2nd year</option>
					                					<option value="3rd">3rd year</option>
					                					<option value="4th">4th year</option>
					                					<option value="5th">5th year</option>
	                								<?php elseif($rows['year_lvl'] == '2nd'): ?>
	                									<option value="1st">1st year</option>
	                									<option value="2nd" selected='selected'>2nd year</option>
	                									<option value="3rd">3rd year</option>
					                					<option value="4th">4th year</option>
					                					<option value="5th">5th year</option>
                									<?php elseif($rows['year_lvl'] == '3rd'): ?>
	                									<option value="1st">1st year</option>
	                									<option value="2nd">2nd year</option>
                										<option value="3rd" selected='selected'>3rd year</option>
                										<option value="4th">4th year</option>
					                					<option value="5th">5th year</option>
            										<?php elseif($rows['year_lvl'] == '4th'): ?>
            											<option value="1st">1st year</option>
	                									<option value="2nd">2nd year</option>
                										<option value="3rd">3rd year</option>
            											<option value="4th" selected='selected'>4th year</option>
            											<option value="5th">5th year</option>
        											<?php elseif($rows['year_lvl'] == '5th'): ?>
        												<option value="1st">1st year</option>
	                									<option value="2nd">2nd year</option>
                										<option value="3rd">3rd year</option>
            											<option value="4th">4th year</option>
        												<option value="5th" selected='selected'>5th year</option>
				                					<?php endif ?>
				                				</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Position:</label>
											</div>
											<div class="col-md-9">
												<select class="form-control" name="pos" required="required">
													<option value="">Choose Position . . .</option>
													<?php if ($result2->num_rows > 0): ?>
														<?php while($rows2 = $result2->fetch_assoc()): ?>
															<?php if($rows2['pos_id'] == $rows['pos_id']): ?>
																<option value="<?=$rows['pos_id']?>" selected='selected'><?=$rows['pos_type']?></option>
															<?php else: ?>
																<option value="<?=$rows2['pos_id']?>"><?=$rows2['pos_type']?></option>
															<?php endif; ?>
														<?php endwhile; ?>
													<?php endif ?>
				                				</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-3 text-right">
												<label>Party:</label>
											</div>
											<div class="col-md-9">
												<select class="form-control" name="party" required="required">
													<option value="">Choose Party . . .</option>
													<?php if ($rows['party'] == 'WISELY'): ?>
														<option value="WISELY" selected='selected'>WISELY</option>
														<option value="CHANGES">CHANGES</option>
													<?php elseif($rows['party'] == 'CHANGES'): ?>
														<option value="WISELY">WISELY</option>
														<option value="CHANGES" selected='selected'>CHANGES</option>
													<?php endif ?>
				                				</select>
											</div>
										</div>
									</div>
					</div>
					<div class="card-footer text-right">
						<a href="../candidates_list.php" class="btn btn-dark">Cancel</a>
						<button type="submit" name="update" class="btn btn-primary">Update changes</button>
						</form>
					</div>
				</div>

				<div class="card mt-3">
					<div class="card-header">Update Image</div>
					<div class="card-body">
						<form method="POST" action="update_candidate_image.php"  enctype="multipart/form-data">
							<div class="form-group">
								<input type="hidden" name="can_id" value="<?=$rows['can_id']?>">
								<input type="hidden" name="img_name" value="<?php echo $rows['image']?>">
							<div class="row">
								
								<div class="col-md-3 text-right">
									<label>Candidate's Photo:</label>
								</div>
								<div class="col-md-9">
									<?php if (empty($rows['image'])): ?>
											<img src="../../uploads/no-image.png" width="130" height="135">
									<?php else: ?>
											<img src="../../uploads/<?=$rows['image']?>" width="130" height="135">
									<?php endif ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-3 text-right">
									<label>Upload Photo:</label>
								</div>
								<div class="col-md-9">
									<input type="file" name="update_img" accept="image/x-png, image/gif, image/jpeg">
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer text-right">
						<a href="../candidates_list.php" class="btn btn-dark">Cancel</a>
						<button type="submit" name="update" class="btn btn-primary">Update photo</button>
						</form>
					</div>
				</div>

				<?php endwhile; ?>
			<?php endif; ?>
			</div>
		</div>
	</div>

<?php include('../../includes/footer.php'); ?>