<?php 
	
	session_start();
	include '../verify/include/global.php';
	include '../includes/function.php';
	include '../includes/header.php'; 

?>

<?php if(empty($_SESSION)) { $_SESSION['is_logged_in'] = 0; }  ?>

<?php if (!is_null($_SESSION['is_logged_in']) && isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] == 1): ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 bread-fluid">
				<nav aria-label="breadcrumb">
				    <ol class="breadcrumb">
				    	<li><b>You are currently here:</b></li>
					    <li class="breadcrumb-item active" aria-current="page">&nbsp&nbspDashboard</li>
				    </ol>
				</nav>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="card text-center">
					<div class="card-header bg-light"><span class="fa fa-user"></span> Total Voters</div>
					<div class="card-body">
						<h1>
							<?php  
								$totVoters = countVaC('sdssu_voters');
								echo $totVoters;
							?>
						</h1>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="card text-center">
					<div class="card-header bg-light"><span class="fa fa-user"></span> Total Candidates</div>
					<div class="card-body">
						<h1>
							<?php  
								$totCan = countVaC('sdssu_candidates');
								echo $totCan;
							?>
						</h1>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="card text-center">
					<div class="card-header bg-light"><span class="fa fa-thumbs-up"></span> Total Voted</div>
					<div class="card-body">
						<h1>
							<?php  
								$totVoted = countVoted('sdssu_candidates');
								echo $totVoted;
							?>
						</h1>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="card text-center">
					<div class="card-header bg-light"><span class="fa fa-thumbs-down"></span> Total Unvoted</div>
					<div class="card-body">
						<h1>
							<?php  
								$totUnvoted = countUnvoted('sdssu_candidates');
								echo $totUnvoted;
							?>
						</h1>
					</div>
				</div>
			</div>
		</div>
		
		<!-- <div class="row mt-5">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h3 class="text-center"><b>President</b></h3>
						<p>
							<?php 
								$pos_num = 1;
								$president = getPresident();
							 ?>

							 <?php if (count($president) > 0): ?>
						 			<?php foreach ($president as $pres): ?>
						 					<img src="../uploads/<?=$pres['image']?>" class="rounded-circle shadow mt-4 ml-5" width="150" height="150">
						 			<?php endforeach ?>
							 <?php endif ?>
						</p>
					</div>
				</div>
			</div>
		</div> -->
	</div>
<?php else: ?>
		<?php include '../includes/403error.php'; ?> 
<?php endif ?>

<?php include('../includes/footer.php'); ?>