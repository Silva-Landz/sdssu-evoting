<?php 
	
	session_start();	

	include '../verify/include/global.php';

	$sql      = "SELECT * FROM sdssu_voters INNER JOIN sdssu_degrees ON sdssu_voters.deg_id = sdssu_degrees.deg_id";
	$result   = $conn->query($sql);

	$sql_deg  = "SELECT * FROM sdssu_degrees"; 
	$result1  = $conn->query($sql_deg);

 ?>

<?php include '../includes/header.php'; ?>

<?php if (!is_null($_SESSION['is_logged_in']) && isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] == 1): ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 bread-fluid">
			<nav aria-label="breadcrumb">
			    <ol class="breadcrumb">
			    	<li><b>You are currently here:</b> </li>
				    <li class="breadcrumb-item active" aria-current="page">&nbsp&nbspVoters List</li>
			    </ol>
			</nav>
		</div>
	</div>
</div>

<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if (empty($_SESSION['success'])): ?>
					<div></div>
				<?php elseif($_SESSION['success'] == 'YES'): ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<strong>Successfully!</strong> Inserted/Updated a voter.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					 		<span aria-hidden="true">&times;</span>
						</button>
					</div>
				<?php elseif($_SESSION['success'] == 'NO'): ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<strong>Error!</strong> Something went wrong.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					 		<span aria-hidden="true">&times;</span>
						</button>
					</div>

				<?php endif ?>
			</div>
		</div>
	<div class="row mt-2">
		<div class="col-md-6">

			<!-- Add modal for creating a new candidate -->
			<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addCandidate"><span class="fa fa-plus-circle"></span> New voter</a>
			<!-- Modal -->
			<div class="modal fade" id="addCandidate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			    <div class="modal-dialog modal-lg" role="document">
			        <div class="modal-content">
			            <div class="modal-header">
			                <h5 class="modal-title" id="exampleModalLabel">Add a new voter</h5>
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                    <span aria-hidden="true">&times;</span>
			                </button>
			            </div>
			            <div class="modal-body">
			            	<!-- Content here -->
			                <div class="container">
			                	<!-- <div class="row"> -->
			                		<div class="col-md-12">
			                		<!-- Creating a new candidate form -->
			                		<form method="POST" action="action/add_voter.php">
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>First Name:</label>
			                				</div>
										    <div class="col-sm-9">
										    	<input type="text" class="form-control" name="fname"  aria-describedby="emailHelp" placeholder="First name" required="required">
										    </div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Last Name:</label>
			                				</div>
										    <div class="col-sm-9">
										    	<input type="text" class="form-control" name="lname"  aria-describedby="emailHelp" placeholder="Last name" required="required">
										    </div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Middle Name:</label>
			                				</div>
										    <div class="col-sm-9">
										    	<input type="text" class="form-control" name="mname"  aria-describedby="emailHelp" placeholder="Middle name" required="required">
										    </div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Gender:</label>
			                				</div>
			                				<div class="col-sm-9">
			                					<select class="form-control" name="gender" required="required">
				                					<option value="">Choose gender . . .</option>
				                					<option value="Male">Male</option>
				                					<option value="Female">Female</option>
				                				</select>
			                				</div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Degree Program:</label>
			                				</div>
			                				<div class="col-sm-9">
			                					<select class="form-control" name="degree" required="required">
				                					<option value="">Choose degree program . . .</option>
				                					<?php if ($result1->num_rows != 0): ?>
				                						<?php while($rows = $result1->fetch_assoc()): ?>
				                							<option value="<?=$rows['deg_id']?>"><?=$rows['deg_name']?></option>
			                							<?php endwhile; ?>
				                					<?php endif ?>
			                					</select>
			                				</div>
			                			</div>
			                			<div class="form-group row">
			                				<div class="col-sm-3 text-right">
			                					<label>Year Level:</label>
			                				</div>
			                				<div class="col-sm-9">
			                					<select class="form-control" name="yearlvl" required="required">
			                					<option value="">Choose year level . . .</option>
			                					<option value="1st">1st year</option>
			                					<option value="2nd">2nd year</option>
			                					<option value="3rd">3rd year</option>
			                					<option value="4th">4th year</option>
			                					<option value="5th">5th year</option>
			                				</select>
			                				</div>
			                			</div>
			                	</div>
			                	<!-- </div> -->
			                </div>
			            </div>
			            <div class="modal-footer">
			                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			                <button type="submit" class="btn btn-primary">Save changes</button>
	                		</form>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
		<div class="col-md-6 text-right">
			<a href="../register" class="btn btn-outline-success"><span class="fa fa-thumbs-o-up"></span> Finger print manager</a>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col-md-12">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
			    <li class="nav-item">
			        <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true">All Voters</a>
			    </li>
			</ul>
			<div class="tab-content mt-3" id="myTabContent">
			    <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
					<table id="all_voters" class="table table-striped" style="width:100%">
				        <thead>
				            <tr>
				                <th>Voter's i.d</th>
				                <th>Fullname</th>
				                <th>Gender</th>
				                <th>Course and Year</th>
				                <th>Status</th>
				                <th></th>
				                <th></th>
				            </tr>
				        </thead>
				        <tbody>
				        	<?php if ($result->num_rows != 0): ?>
				        		<?php while($rows1 = $result->fetch_assoc()): ?>
				        				<tr>
							                <td><?=$rows1['voters_id']?></td>
							                <td><?=$rows1['fname']." ".$rows1['mname']." ".$rows1['lname']?></td>
							                <td><?=$rows1['gender']?></td>
							                <td><?=$rows1['deg_code']." - ".$rows1['year_lvl']." year"?></td>
							                <td class="text-center">
							                	<?php if ($rows1['status'] == 0): ?>
							                			<div class="badge badge-danger">Unvoted</div>
					                			<?php elseif($rows1['status'] == 1): ?>
						                				<div class="badge badge-success">Voted</div>
							                	<?php endif; ?>
						                	</td>
						                	<!-- <td class="text-center">
						                		<?php if ($rows1['finger_id'] == 0): ?>
							                			<div class="badge badge-danger">Unregistered</div>
					                			<?php elseif($rows1['finger_id'] == 1): ?>
						                				<div class="badge badge-success">Registered</div>
							                	<?php endif; ?>
						                	</td> -->
							                <td><a href="action/edit_voter.php?voter_id=<?=$rows1['voters_id']?>" class="btn btn-info"><span class="fa fa-pencil"></span></a></td>
							                <td>
												<a href="javascript:void(0)" class="btn btn-danger" name="delete_voter" id="delete_voter" data-id="<?=$rows1['voters_id']?>"><span class="fa fa-trash"></span></a>
							                </td>
							            </tr>
				        		<?php endwhile; ?>
				        	<?php endif; ?>
				        </tbody>
				    </table>
					<!-- add datatables -->
			    </div>
			</div>
		</div>
	</div>
</div>

<?php else: ?>
	<?php include '../includes/403error.php'; ?>
<?php endif; ?>

<?php 
	include('../includes/footer.php');
	unset($_SESSION['success']);
 ?>