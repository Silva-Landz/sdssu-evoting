<?php 

	session_start();

	include '../includes/header.php'; ?>
	
	<div class="container-fluid">
		<div class="row text-center mt-5">
			<div class="col-md-12">
				<img src="../assets/images/sdssulogo.png" width="180" height="160">
				<h1 class="main_title mt-3">SDSSU AUTOMATED VOTING SYSTEM</h1>
				<p class="lead">SURIGAO DEL SUR STATE UNIVERSITY</p>
			</div>
		</div>

		<div class="row mt-4 justify-content-center">
			<div class="col-md-3">
				<form method="POST" action="action/login.php">
					<div class="form-group">
						<label><i class="fa fa-user"></i>&nbspUsername</label>
						<input type="text" name="username" class="form-control" placeholder="Username">
					</div>
					<div class="form-group">
						<label><i class="fa fa-lock"></i>&nbspPassword</label>
						<input type="password" name="passwrd" class="form-control" placeholder="Password">
					</div>
					<button type="submit" class="btn btn-success form-control">Login</button>
				</form>
			</div>
		</div>

<?php include('../includes/footer.php'); ?>