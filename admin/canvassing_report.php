<?php 

	session_start();
	
	include '../verify/include/global.php';
	include '../includes/function.php';

	include '../includes/header.php'; 

?>

<?php if (!is_null($_SESSION['is_logged_in']) && isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] == 1): ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 bread-fluid">
			<nav aria-label="breadcrumb">
			    <ol class="breadcrumb">
			    	<li><b>You are currently here:</b> </li>
				    <li class="breadcrumb-item active" aria-current="page">&nbsp&nbspCanvassing Report</li>
			    </ol>
			</nav>
		</div>
	</div>
</div>

<div class="container">
	<div class="row mt-3 ml-1">
		<div class="col-md-12">
			<form method="POST" action="action/download_canvass.php">
				<button class="btn btn-outline-success"><span class="fa fa-download"></span> Download</button>
			</form>
		</div>
	</div>
	<div class="row mt-4">
		<div class="col-md-12">
			<table id="canvassing_report" class="table table-striped" style="width:100%">
				<thead>
		            <tr>
		            	<th>#</th>
		            	<th class="text-center">Photo</th>
		                <th class="text-center">Position</th>
		                <th class="text-center">Fullname</th>
		                <th class="text-center">Party</th>
		                <th class="text-center">Gender</th>
		                <th class="text-center">Course & Year</th>
		                <th class="text-center">Total Votes</th>
		            </tr>
		        </thead>
		        <tbody>
					<?php 
						$sql = "SELECT * FROM sdssu_candidates sc INNER JOIN sdssu_degrees sd ON sc.deg_id = sd.deg_id INNER JOIN sdssu_positions sp 
									ON sc.pos_id = sp.pos_id ORDER BY sc.pos_id ASC";
						$result = $conn->query($sql);
					?>
						<?php 
							$i = 1;
							while($rows = $result->fetch_assoc()): ?>
								<tr>
					        		<td><?=$i;?></td>
					        		<td><img src="../uploads/<?=$rows['image']?>" width="40" height="40"></td>
					        		<td><?=$rows['pos_type']?></td>
					        		<td><?=$rows['lname'].", ".$rows['fname']." ".$rows['mname']?></td>
					        		<td><?=$rows['party']?></td>
					        		<td><?=$rows['gender']?></td>
					        		<td><?=$rows['deg_name']?></td>
					        		<td class="text-center">
					        			<?php 
					        				$sqlVotes = "SELECT * FROM sdssu_votes WHERE can_id='".$rows['can_id']."' AND total_votes=1";
					        				$resVotes = $conn->query($sqlVotes);

				        					echo $resVotes->num_rows;
					        			 ?>
					        		</td>
					        	</tr>
						<?php 
							$i++;
							endwhile; ?>					        	
		        	
		        </tbody>
			</table>
		</div>
	</div>
</div>

<?php else: ?>
	<?php include '../includes/403error.php'; ?>
<?php endif; ?>

<?php include('../includes/footer.php'); ?>