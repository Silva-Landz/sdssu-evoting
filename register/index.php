<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
	<head>
		<?php include 'include/global.php'; ?>
		<?php include 'include/head.php'; ?>
	</head>
	<body>

		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
			<a class="navbar-brand" href="../admin/dashboard.php"><img src="assets/image/sdssulogo.png" width="45" height="43"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
			    <ul class="navbar-nav">
			    	<li class="nav-item">
			        	<a class="nav-link text-light" href="#" onclick="load('<?php echo $base_path?>user.php?action=index')"><span class="fa fa-user"></span> Voters</a>
			      	</li>
			      	<li class="nav-item">
			        	<a class="nav-link text-light" href="#" onclick="load('<?php echo $base_path?>log.php?action=index')"><span class="fa fa-history"></span> History logs</a>
			      	</li>
			    </ul>
			    <ul class="nav navbar-nav navbar-right">
			    	<li class="nav-item"><a href="../admin/voters_list.php" class="btn btn-outline-light">Back</a></li>
			    </ul>
			</div>
		</nav>
	<?php session_start(); ?>
	<?php if (!is_null($_SESSION['is_logged_in']) && isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] == 1): ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!-- Loads the login -->
					<div id="content"></div>
					<!-- End login -->
				</div>
			</div>
		</div>

	<script>
		jQuery(document).ready(function() {

			console.log('ready to use...');

			load('<?php echo $base_path?>user.php?action=index');

		});
	</script>
	<?php else: ?>
		<?php include '../includes/403error.php'; ?>
	<?php endif ?>
	</body>
</html>