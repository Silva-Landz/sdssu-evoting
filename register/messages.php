<?php

session_start();
	
if (isset($_GET['msg']) && !empty($_GET['msg'])) {
	
	echo $_GET['msg'];

} elseif (isset($_GET['user_id']) && !empty($_GET['user_id']) && isset($_GET['time']) && !empty($_GET['time'])) {
	
	include 'include/global.php';
	include 'include/function.php';
	include 'include/head.php';

	$sql = "SELECT * FROM demo_user a INNER JOIN demo_finger b ON a.user_id=b.user_id INNER JOIN sdssu_voters c ON 
				a.user_id=c.voters_id WHERE a.user_id='".$_GET['user_id']."' LIMIT 1";

	$result = $conn->query($sql);
	$rows = $result->fetch_assoc();

?>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="jumbotron">
					<h1 class="display-4">Hello, <?=$rows['fname']?>!</h1>
					<p class="lead">Welcome to SDSSU Cantilan Voting System. This is to inform you that you have successfully logged in your account.</p>
					<hr class="my-4">
					<p class="lead">
					 	<?php if ($rows['status'] == 1): ?>
					 			<p class="lead alert alert-danger"><span class="fa fa-warning"></span> You can only vote once!</p>
					 			<a href="../verify" class="btn btn-dark btn-lg pull-center"><span class="fa fa-home"></span> Home</a>
			 			<?php else: ?>
			 					<a class="btn btn-primary btn-lg" href="../voting.php?voters_id=<?=$rows['voters_id']?>&voters_name=<?=$rows['fname']?>&status=<?=$rows['status']?>" role="button">Proceed to voting</a>
					 	<?php endif ?>
					    
					</p>
				</div>
			</div>
		</div>
	</div>


<?php
	
} else {
		
	$msg = "Parameter invalid..";
	
	echo "$msg";
	
}

	
?>