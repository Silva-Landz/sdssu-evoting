<?php 
	
	include 'verify/include/global.php';
	include 'includes/function.php';
	include('includes/header.php'); 

?>

<div class="container-fluid bread-fluid">
	<nav class="navbar navbar-dark bg-primary">
		<a class="navbar-brand" href="/sdssuevote">Logo here</a>
	</nav>
</div>


<div class="container">
	<div class="row mt-5 justify-content-center">
		<div class="col-md-10">
			<div class="card">
				<div class="card-header">
					Candidate's Information
				</div>
				<div class="card-body">
					<div class="row">
						<?php $profile = getSpecificCan($_GET['id']); ?>
						<?php if (count($profile) > 0): ?>
								<?php foreach ($profile as $rows): ?>
										<div class="col-sm-3">
											<img src="uploads/<?=$rows['image']?>" width="200" height="210">
										</div>
										<div class="col-sm-2 text-left">
											<p><b>Name:</b></p>
											<p><b>Position:</b></p>
											<p><b>Gender:</b></p></p>
											<p><b>Course & Year:</b></p>
											<p><b>Party:</b></p>
										</div>
										<div class="col-sm-6">
											<p><?=$rows['fname']." ".$rows['mname']." ".$rows['lname']?></p>
											<p><?=$rows['pos_type']?></p>
											<p><?=$rows['gender']?></p></p>
											<p><?=$rows['deg_name']." - ".$rows['year_lvl']?> year</p></p>
											<p><?=$rows['party']?></p>
										</div>
								<?php endforeach ?>
						<?php endif ?>
					</div>

					<div class="row mt-5">
						<div class="col-sm-12">
							<button type="button" class="btn btn-primary" onclick="history.back();"><span class="fa fa-arrow-left"></span> Back</button>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>


<?php include('includes/footer.php'); ?>