<?php 
	session_start();

	include 'verify/include/global.php';
	include 'includes/function.php';
?>

<?php include('includes/header.php'); ?>

<div class="container-fluid bread-fluid">
	<nav class="navbar navbar-dark bg-primary">
		<a class="navbar-brand" href="/sdssuevote">Logo here</a>
	</nav>
</div>

<?php if (!empty($_GET['voters_id']) && isset($_GET['voters_id']) && !empty($_GET['voters_name']) && isset($_GET['status']) && $_GET['status'] == 0): ?>
		<form method="POST" action="review_ballot.php?voters_id=<?=$_GET['voters_id']?>&status=<?=$_GET['status']?>">
		<input type="hidden" name="voters_id" value="<?=$_GET['voters_id']?>">
		<div class="container">
			<div class="row mt-4 mb-5">
				<div class="col-md-6 panel">
					<h3>President</h3>
					<hr>
					<div class="row mx-auto">
						<?php $president = getPresident(); ?>
						<?php if (count($president) > 0): ?>
							<?php foreach ($president as $rows): ?>
									<div class="col-sm-6 mx-auto">
										<div class="card" style="width: 15rem;">
											<div class="card-header card-title text-center">
												<b><?=$rows['fname']." ".$rows["lname"]?></b><br>
												<small class="badge badge-info"><?=$rows['party']?></small>
											</div>
										  	<div class="card-body text-center">
										    	<img src="uploads/<?=$rows['image']?>" class="img-responsive rounded-circle shadow" alt="president" width="150" height="145">
										  	</div>
										  	<div class="card-footer text-center">
										    	<small class="text-muted"><a href="view_profile.php?id=<?=$rows['can_id']?>">View Profile</a></small>
										    </div>
										</div>
									</div>
							<?php endforeach ?>
							<?php else: ?>
								<h3>NO ENTRIES</h3>
						<?php endif ?>	
					</div>
					<div class="row mt-3">
						<div class="col-sm-8 mx-auto">	
					    	<?php if (count($president) > 0): ?>
							    <select class="form-control" name="president">
				    				<option value="">-- Choose a candidate --</option>
				    				<?php foreach ($president as $rows): ?>
						    			<option value="<?=$rows['can_id']?>"><?=$rows['fname']." ".$rows['mname']." ".$rows['lname']?></option>
							    	<?php endforeach ?>
							    </select>
					    	<?php endif ?>	    
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<h3>Vice President - Internal</h3>
					<hr>
					<div class="row mx-auto">
						<?php $viceInternal = getViceInternal(); ?>
						<?php if (count($viceInternal) > 0): ?>
							<?php foreach ($viceInternal as $rows): ?>
									<div class="col-sm-6 mx-auto">
										<div class="card" style="width: 15rem;">
											<div class="card-header card-title text-center">
												<b><?=$rows['fname']." ".$rows["lname"]?></b><br>
												<small class="badge badge-info"><?=$rows['party']?></small>
											</div>
										  	<div class="card-body text-center">
										    	<!-- <h6 class="card-title"><?=$rows['fname']." ".$rows["lname"]?></h6>
										    	<hr> -->
										    	<img src="uploads/<?=$rows['image']?>" class="img-responsive rounded-circle shadow" alt="president" width="150" height="145">
										  	</div>
										  	<div class="card-footer text-center">
										    	<small class="text-muted"><a href="view_profile.php?id=<?=$rows['can_id']?>">View Profile</a></small>
										    </div>
										</div>
									</div>
							<?php endforeach ?>
							<?php else: ?>
								<h3>NO ENTRIES</h3>
						<?php endif ?>	
					</div>
					<div class="row mt-3">
						<div class="col-sm-8 mx-auto">
					    	<?php if (count($viceInternal) > 0): ?>
					    		<select class="form-control" name="viceInternal">
				    				<option value="">-- Choose a candidate --</option>
				    				<?php foreach ($viceInternal as $rows): ?>
						    			<option value="<?=$rows['can_id']?>"><?=$rows['fname']." ".$rows['mname']." ".$rows['lname']?></option>
							    	<?php endforeach ?>
							    </select>
					    	<?php endif ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-3 mb-5">
				<div class="col-md-6">
					<h3>Vice President - External</h3>
					<hr>
					<div class="row mx-auto">
						<?php $viceExternal = getViceExternal(); ?>
						<?php if (count($viceExternal) > 0): ?>
							<?php foreach ($viceExternal as $rows): ?>
									<div class="col-sm-6 mx-auto">
										<div class="card" style="width: 15rem;">
											<div class="card-header card-title text-center">
												<b><?=$rows['fname']." ".$rows["lname"]?></b><br>
												<small class="badge badge-info"><?=$rows['party']?></small>
											</div>
										  	<div class="card-body text-center">
										    	<!-- <h6 class="card-title"><?=$rows['fname']." ".$rows["lname"]?></h6>
										    	<hr> -->
										    	<img src="uploads/<?=$rows['image']?>" class="img-responsive rounded-circle shadow" alt="president" width="150" height="145">
										  	</div>
										  	<div class="card-footer text-center">
										    	<small class="text-muted"><a href="view_profile.php?id=<?=$rows['can_id']?>">View Profile</a></small>
										    </div>
										</div>
									</div>
							<?php endforeach ?>
							<?php else: ?>
								<h3>NO ENTRIES</h3>
						<?php endif ?>	
					</div>
					<div class="row mt-3">
						<div class="col-sm-8 mx-auto">
					    	<?php if (count($viceExternal) > 0): ?>
					    		<select class="form-control" name="viceExternal">
				    				<option value="">-- Choose a candidate --</option>
				    				<?php foreach ($viceExternal as $rows): ?>
						    			<option value="<?=$rows['can_id']?>"><?=$rows['fname']." ".$rows['mname']." ".$rows['lname']?></option>
							    	<?php endforeach ?>
							    	</select>
					    	<?php endif ?> 
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<h3>Secretary</h3>
					<hr>
					<div class="row mx-auto">
						<?php $secretary = getSecretary(); ?>
						<?php if (count($secretary) > 0): ?>
							<?php foreach ($secretary as $rows): ?>
									<div class="col-sm-6 mx-auto">
										<div class="card" style="width: 15rem;">
											<div class="card-header card-title text-center">
												<b><?=$rows['fname']." ".$rows["lname"]?></b><br>
												<small class="badge badge-info"><?=$rows['party']?></small>
											</div>
										  	<div class="card-body text-center">
										    	<!-- <h6 class="card-title"><?=$rows['fname']." ".$rows["lname"]?></h6>
										    	<hr> -->
										    	<img src="uploads/<?=$rows['image']?>" class="img-responsive rounded-circle shadow" alt="president" width="150" height="145">
										  	</div>
										  	<div class="card-footer text-center">
										    	<small class="text-muted"><a href="view_profile.php?id=<?=$rows['can_id']?>">View Profile</a></small>
										    </div>
										</div>
									</div>
							<?php endforeach ?>
							<?php else: ?>
								<h3>NO ENTRIES</h3>
						<?php endif ?>	
					</div>
					<div class="row mt-3">
						<div class="col-sm-8 mx-auto">
					    	<?php if (count($secretary) > 0): ?>
					    		<select class="form-control" name="secretary">
				    				<option value="">-- Choose a candidate --</option>
				    				<?php foreach ($secretary as $rows): ?>
						    			<option value="<?=$rows['can_id']?>"><?=$rows['fname']." ".$rows['mname']." ".$rows['lname']?></option>
							    	<?php endforeach ?>
							    	</select>
					    	<?php endif ?> 
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-3 mb-5">
				<div class="col-md-6">
					<h3>Treasurer</h3>
					<hr>
					<div class="row mx-auto">
						<?php $treasurer = getTreasurer(); ?>
						<?php if (count($treasurer) > 0): ?>
							<?php foreach ($treasurer as $rows): ?>
									<div class="col-sm-6 mx-auto">
										<div class="card" style="width: 15rem;">
											<div class="card-header card-title text-center">
												<b><?=$rows['fname']." ".$rows["lname"]?></b><br>
												<small class="badge badge-info"><?=$rows['party']?></small>
											</div>
										  	<div class="card-body text-center">
										    	<!-- <h6 class="card-title"><?=$rows['fname']." ".$rows["lname"]?></h6>
										    	<hr> -->
										    	<img src="uploads/<?=$rows['image']?>" class="img-responsive rounded-circle shadow" alt="president" width="150" height="145">
										  	</div>
										  	<div class="card-footer text-center">
										    	<small class="text-muted"><a href="view_profile.php?id=<?=$rows['can_id']?>">View Profile</a></small>
										    </div>
										</div>
									</div>
							<?php endforeach ?>
							<?php else: ?>
								<h3>NO ENTRIES</h3>
						<?php endif ?>	
					</div>
					<div class="row mt-3">
						<div class="col-sm-8 mx-auto">
					    	<?php if (count($treasurer) > 0): ?>
					    		<select class="form-control" name="treasurer">
				    				<option value="">-- Choose a candidate --</option>
				    				<?php foreach ($treasurer as $rows): ?>
						    			<option value="<?=$rows['can_id']?>"><?=$rows['fname']." ".$rows['mname']." ".$rows['lname']?></option>
							    	<?php endforeach ?>
							    	</select>
					    	<?php endif ?> 
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<h3>Auditor</h3>
					<hr>
					<div class="row mx-auto">
						<?php $auditor = getAuditor(); ?>
						<?php if (count($auditor) > 0): ?>
							<?php foreach ($auditor as $rows): ?>
									<div class="col-sm-6 mx-auto">
										<div class="card" style="width: 15rem;">
											<div class="card-header card-title text-center">
												<b><?=$rows['fname']." ".$rows["lname"]?></b><br>
												<small class="badge badge-info"><?=$rows['party']?></small>
											</div>
										  	<div class="card-body text-center">
										    	<!-- <h6 class="card-title"><?=$rows['fname']." ".$rows["lname"]?></h6>
										    	<hr> -->
										    	<img src="uploads/<?=$rows['image']?>" class="img-responsive rounded-circle shadow" alt="president" width="150" height="145">
										  	</div>
										  	<div class="card-footer text-center">
										    	<small class="text-muted"><a href="view_profile.php?id=<?=$rows['can_id']?>">View Profile</a></small>
										    </div>
										</div>
									</div>
							<?php endforeach ?>
							<?php else: ?>
								<h3>NO ENTRIES</h3>
						<?php endif ?>	
					</div>
					<div class="row mt-3">
						<div class="col-sm-8 mx-auto">
					    	<?php if (count($auditor) > 0): ?>
					    		<select class="form-control" name="auditor">
				    				<option value="">-- Choose a candidate --</option>
				    				<?php foreach ($auditor as $rows): ?>
						    			<option value="<?=$rows['can_id']?>"><?=$rows['fname']." ".$rows['mname']." ".$rows['lname']?></option>
							    	<?php endforeach ?>
							    	</select>
					    	<?php endif ?> 
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-3">
				<div class="col-md-12">
					<h3>Senators</h3>
					<hr>
					<p class="alert alert-info"><span class="fa fa-info-circle"></span><b> NOTE: </b>You can only choose not more than 8 senators.</p>

					<?php 

						$i = 1;
						$senators = getSentor();
						if (count($senators) > 0): 

					?>
						<div class="row">
						<input class="form-check-input" type="hidden" name="senator[]" value="0">	
						<?php foreach ($senators as $rows): ?>
								
									<div class="col-sm-3 mb-5">
										<div class="card" style="width: 15rem;">
										  	<div class="card-body text-center">
										    	<h6 class="card-title"><?=$rows['fname']." ".$rows["lname"]?></h6>
										    	<small class="badge badge-info"><?=$rows['party']?></small>
										    	<hr>
										    	<img src="uploads/<?=$rows['image']?>" class="img-responsive rounded-circle shadow" alt="president" width="150" height="145">
										  	</div>
										  	<div class="card-footer text-center">
										    	<small class="text-muted"><a href="view_profile.php?id=<?=$rows['can_id']?>">View Profile</a></small>
										    </div>
										</div>
									    <div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" name="senator[]" value="<?=$rows['can_id']?>">
											<label class="form-check-label" for="inlineCheckbox1">Senator <?=$i?></label>
										</div>
									</div>
								
								<?php $i++ ?>
						<?php endforeach ?>
						</div>
					<?php else: ?>
							<h3 class="alert alert-notice">NO ENTRIES</h3>
					<?php endif ?>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-md-6">
					<a href="../sdssuevote" class="btn btn-outline-primary"><span class="fa fa-arrow-left"></span> Vote Later</a>
				</div>
				<div class="col-md-6 text-right">
					<button type="submit" class="btn btn-outline-primary">Proceed for final review <span class="fa fa-arrow-right"></span></button>
				</div>
			</div>
		</div>
		</form>
		
<?php else: ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h1 class="display-1">404 Error</h1>
					<h2><b>PAGE NOT FOUND</b></h2>
					<h5 class="text-muted mt-4">We're sorry, the page you have requested could not be found. Please go back to the homepage or contact the programmer.</h5>
					<p class="mt-5">
						<a href="../sdssuevote/verify" class="btn btn-dark btn-lg"><span class="fa fa-home"></span> Home</a>
					</p>
				</div>
			</div>
		</div>
<?php endif ?>

<?php include('includes/footer.php'); ?>