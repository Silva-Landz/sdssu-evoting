<?php include 'includes/header.php'; ?>

<script src="assets/js/jquery.js" crossorigin="anonymous"></script>
<script src="assets/js/sweetalert2.all.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function() {
	        swal({
	            title: "Success!",
	            text: "Thank you for voting",
	            type: "success",
	            allowOutsideClick: false,
	            showConfirmButton: true
	        }).then(function(isConfirm){
	        	if (isConfirm) {
	        		window.location = "verify";
	        	} else {
	        		alert("No");
	        	}
	        });
	    }, 100);
	});
</script>
