<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
	<head>
		<?php include 'include/global.php'; ?>
		<?php include 'include/head.php'; ?>
	</head>
	<body style="background-color: #076794; color: white;">

		<div class="container">
			<div class="row text-center">
				<div class="col-md-12">
					<img src="assets/image/sdssulogo.png" width="180" height="160">
					<h1 class="main_title mt-3">SDSSU AUTOMATED VOTING SYSTEM</h1>
					<p class="lead_title">SURIGAO DEL SUR STATE UNIVERSITY - Cantilan Campus</p>
					
					<!-- Loads the login -->
					<div id="content"></div>
					<!-- End login -->
				</div>
			</div>
		</div>

	<script>
		jQuery(document).ready(function() {

			console.log('ready to use...');

			load('<?php echo $base_path?>login.php?action=index');

		});
	</script>
	</body>
</html>