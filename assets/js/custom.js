$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});

$(document).ready(function() {

	// datatables for candidates list
    $('#all_candidates').DataTable();
    $('#table-president').DataTable();
    $('#table-internal').DataTable();
    $('#table-external').DataTable();
    $('#table-secretary').DataTable();
    $('#table-treasurer').DataTable();
    $('#table-auditor').DataTable();
    $('#table-senator').DataTable();

    // datatables for voters list
    $('#all_voters').DataTable();

    //  canvassing report
    $('#canvassing_report').DataTable();

    //history logs
    $('#history_logs').DataTable();

} );


$(document).ready(function(){

    $(document).on('click', '#delete_candidate', function(e){

        var can_id = $(this).attr('data-id'); 
        SwalDelete(can_id);
        e.preventDefault();
 
    });

    function SwalDelete(id)   {
        swal({
            title: 'Are you sure?',
            text: "It will be deleted permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            showLoaderOnConfirm: true,
              
            preConfirm: function() {
              return new Promise(function(resolve) {
                   
                 $.ajax({
                    url: 'action/delete_candidate.php',
                    type: 'POST',
                    data: 'delete_candidate='+id,
                    dataType: 'json'
                 })
                 .done(function(response){
                    swal('Deleted!', response.message, response.status);
                    realoadPage();
                 })
                 .fail(function(){
                    swal('Oops...', 'Something went wrong with ajax !', 'error');
                 });
              });
            },
            allowOutsideClick: false              
        });
    }

   function realoadPage()   {
        location.reload();
   } 

});

$(document).ready(function(){

    $(document).on('click', '#delete_voter', function(e){

        var voter_id = $(this).attr('data-id');
        SwalDelete(voter_id);
        e.preventDefault();
 
    });

    function SwalDelete(id)   {
        swal({
            title: 'Are you sure?',
            text: "It will be deleted permanently!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            showLoaderOnConfirm: true,
              
            preConfirm: function() {
              return new Promise(function(resolve) {
                   
                 $.ajax({
                    url: 'action/delete_voter.php',
                    type: 'POST',
                    data: 'delete_voter='+id,
                    dataType: 'json'
                 })
                 .done(function(response){
                    swal('Deleted!', response.message, response.status);
                    realoadPage();
                 })
                 .fail(function(){
                    swal('Oops...', 'Something went wrong with ajax !', 'error');
                 });
              });
            },
            allowOutsideClick: false              
        });
    }

   function realoadPage()   {
        location.reload();
   } 

});


// $("#review_ballot").submit(function(e){

//     SwalConfrimVote();
//     e.preventDefault();

//     function SwalConfrimVote()  {
//         swal({
//                     title: "Success",
//                     text: "Thank you for voting",
//                     type: "success",
//                     confirmButtonColor: '#3085d6',
//                     confirmButtonText: 'Ok',
//                     closeOnConfrim: true           
//             }).then(
//                 function(isConfirm)  {
//                     if (isConfirm) {
//                         window.location.href = 'add_votes.php';
//                     } 
//                 }
//             );
//         allowOutsideClick: false
//     }
     
// });
